class Person{
  firstName: string;
  lastName: string;
  age: number;
  birthDate: Date;
}

var p = new Person();
p.firstName = 'John';
p.lastName = 'Doe';
p.age = 21;
p.birthDate = new Date();
alert(JSON.stringify(p));
