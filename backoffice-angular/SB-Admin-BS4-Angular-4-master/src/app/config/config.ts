export const CONFIG = {
    API_URL:'/api',
    PAYIN_URL:'/transactions/payins',
    MERCHANT_URL:'/merchants',
    MERCHANTCODE_URL:'/merchants/code-names',
    PROFILE_URL:'/users/profile',
    PAYOUT_URL:'/transactions/payouts',
    SETTLEMENT_URL:'/transactions/settlements',
    USERS_URL:'/users',
    CURRENT_RATES_URL:'/rates/current',
    MID_CONFIG_URL: '/routing/mid-configs',
    ROUTING_RULE_URL: '/routing/rules',
    PSP_CONFIG_URL: '/psp-configs',
}