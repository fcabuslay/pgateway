import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Http, HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
//import { PayinsComponent } from './layout/payins/payins.component';
//import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';   
import { BackofficeService } from './shared/services/backoffice.service';
import { HttpClientModule } from '@angular/common/http';
//import { InMemoryDataService }  from './in-memory-data.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JsessionIDInterceptor } from './shared/services/jsession.interceptor';
    import { AuthService } from './services/auth.service'
    import { NotifyService } from './services/notify.service'
//import { NotifyComponent } from './notify/notify.component'
//import { HeaderComponent, SidebarComponent } from './shared';
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
    // for development
    // return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-4/master/dist/assets/i18n/', '.json');
    return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}
@NgModule({
    declarations: [
        AppComponent,
        //HeaderComponent,
        //SidebarComponent,
        //PayinsComponent
        //NotifyComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        AppRoutingModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http]
            }
        }),
        HttpClientModule,
        NgbModule.forRoot(),
        // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
        // and returns simulated server responses.
        // Remove it when a real server is ready to receive requests.
        //HttpClientInMemoryWebApiModule.forRoot(
        //InMemoryDataService, { dataEncapsulation: false }
        //),
       
    ],
    providers: [AuthGuard,BackofficeService ,
        {
        provide: HTTP_INTERCEPTORS,
        useClass: JsessionIDInterceptor ,
        multi: true
      },
      AuthService,
      NotifyService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
