import { Injectable,OnInit } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { RequestOptions,Http } from '@angular/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';

@Injectable()
export class LoginService implements OnInit{

    private userURL = 'http://localhost:8882/backoffice/api/login'; 
    //private userURL = 'http://localhost:8881/backoffice/api/login'; 

    constructor(
    private http: HttpClient,
    private router: Router) {
    }

    Login(username: string, password: string) {
        const options = new RequestOptions({
            withCredentials: false,
            params: new HttpParams()
          });
        options.params.set('username',username);
        options.params.set('password',password);
       const headers = new HttpHeaders().set("Content-Type","application/x-www-form-urlencoded");
       const params = new HttpParams()
       .set('username',username)
       .set('password',password);
/*
    return this.http.post<Payin[]>(this.payinsURL, params,{
            headers: headers
          }*/
        return this.http.post<any>(this.userURL, params,{headers: headers, withCredentials: false}).subscribe(res => {
            //console.log(res);
            localStorage.setItem("Admin","1");
            //console.log("login"+localStorage.getItem("Admin"));
            this.router.navigate(['/dashboard']);
          },
          err => {
            localStorage.setItem("Admin","1");
            //console.log("login"+localStorage.getItem("Admin"));
            this.router.navigate(['/dashboard']);
            //this.handleError(err);
          });   
    }

    private handleError(error: any): Observable<any> {
        console.log('An error occurred', error); // for demo purposes only
        return Observable.throw(error);
    }
    ngOnInit(){

    }
}