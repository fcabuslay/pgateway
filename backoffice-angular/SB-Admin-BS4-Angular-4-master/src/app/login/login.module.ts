import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {  
    FormsModule,  
    ReactiveFormsModule  
  } from '@angular/forms';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { LoginService } from './login-service';
import { NotifyModule } from '../notify/notify.module'
import { AuthService } from '../services/auth.service'
import { NotifyService } from '../services/notify.service'

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule,
        FormsModule,        
        ReactiveFormsModule,
        NotifyModule
    ],
    declarations: [LoginComponent   ],
    providers: [ LoginService ,AuthService,NotifyService],
    bootstrap: [ LoginComponent ]
})
export class LoginModule {
}
