import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormControl, FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginService} from './login-service';
import { BackofficeService } from '../shared/services/backoffice.service';
import { HttpClient, HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()],
    providers: [LoginService],
})
export class LoginComponent implements OnInit {
    LoginForm: FormGroup;
    username: string;
    password: string;
    post:any; 
    haserrors:any;
    private url:string;

    
    constructor(
        private router: Router,
        private fb: FormBuilder,
        private ls:LoginService,
        private bs: BackofficeService,
        private http: HttpClient,
        private route: ActivatedRoute,
        private authService: AuthService,
    ) {
/*
            this.LoginForm = fb.group({
                'username' : '',
                'password' : '', f     
              }); 
              */
             this.LoginForm = this.fb.group({
                user: [null, Validators.required],
                pass: [null, Validators.required],
                hide: [null],
                gateway: [null]
              });
    }

    ngOnInit() : void {
        this.generateURL();
        localStorage.clear();
        this.checkParam();
    }
    checkParam(){
        this.haserrors = this.route.snapshot.queryParamMap.get('err');
    }

    generateURL(){
        this.url = this.bs.getLoginURL();
      }

    Login(post) {
        
        let username = post.user;
        let password = post.pass;
        /*
        this.Login2(this.username,this.password);
        //this.ls.Login(this.username,this.password);
        localStorage.setItem('isLoggedin', 'true');*/
        this.authService.login(username,password)
    }

    Login2(username: string, password: string) {
       const headers = new HttpHeaders().set("Content-Type","application/x-www-form-urlencoded");
       const params = new HttpParams()
       .set('username',username)
       .set('password',password);
/*
    return this.http.post<Payin[]>(this.payinsURL, params,{
            headers: headers
          }*/
        return this.http.post<any>(this.url, params,{headers: headers}).subscribe(
            ( data: any) =>  {
            //console.log(res);
            localStorage.setItem("role",data.role.name);
            localStorage.setItem("roleid",data.role.id);
            localStorage.setItem("id",data.id);
            localStorage.setItem("username",data.username);
            //one below is for getting balance of merchant
             
            localStorage.setItem("merchant", JSON.stringify(data.merchants));
            
            this.router.navigate(['/dashboard']);
          },
          (err: HttpErrorResponse) => {
            if (err.status == 401) {    
                this.LoginForm.controls['hide'].setErrors({invalid: true});
                this.router.navigate(['/login']);
            }

            if (err.status == 400) {
                this.LoginForm.controls['hide'].setErrors({invalid: true});
                this.router.navigate(['/login']);
            }

            if ( err.status == 504){
                this.LoginForm.controls['gateway'].setErrors({invalid: true});
                this.router.navigate(['/login']);
            }
            else {                
                this.LoginForm.controls['gateway'].setErrors({invalid: true});
                this.router.navigate(['/login']);
            }
          });
    }

}
