import {  OnInit,Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NotifyService } from '../../services/notify.service';
import { Router } from '@angular/router';

@Injectable()
export class JsessionIDInterceptor implements HttpInterceptor {

    
    constructor(
        private notifyService: NotifyService,
        private router: Router,
    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            withCredentials: true
        });
        return next.handle(request);

        /*
        return next.handle(request).do((event: HttpEvent<any>) => {
            next.handle(request)
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                // do error handling here
                this.authService.handleError(err)
            }
        });
        
        return next.handle(request)
            .do(event => {

            })
            .catch(err => {
                console.log('Caught error', err);
                this.authservice.handleError(err)
                return Observable.throw(err);
            });
        */
            /*
        return next.handle(request).do((event: HttpEvent<any>) => {

              }, (error: any) => {
                if (error instanceof HttpErrorResponse) {
                    if (error.status == 500) {    
                        this.router.navigate(['/login']);
                      }
                  
                      if (error.status == 401) {    
                        this.router.navigate(['/login']);
                      }
                  
                      if (error.status == 400) {    
                        this.notifyService.notify(error.error.error, 'error')
                      }
                  
                      else {                
                        this.router.navigate(['/login']);
                    }
                }
              });            
              */
    }
}