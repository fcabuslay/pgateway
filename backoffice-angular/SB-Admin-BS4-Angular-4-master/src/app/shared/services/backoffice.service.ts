import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { response } from './response';
import { Router } from '@angular/router';

@Injectable()
export class BackofficeService{
    //private backofficeURL = 'http://localhost:8882/backoffice/api';
    private backofficeURL = '/api';
    private settlementURL = '/transactions/settlements';
    private payinsURL = '/transactions/payins';
    private payoutsURL = '/transactions/payouts';
    private usersURL = '/users';
    private loginURL = '/login';
    private merchantsURL = '/merchants';
    private merchantCodeNamesURL = '/merchants/code-names';
    private totalCount:number;
    private merchantCurrentRateURL = '/rates/current';

    constructor(private http: HttpClient,private router: Router,) {
    
     }

    public getURL(){
        return this.backofficeURL;
    }

    public getSettlementURL(){
        const settlement = this.backofficeURL + this.settlementURL;
        return settlement;
    }

    public getPayinURL(){
        const payin = this.backofficeURL + this.payinsURL;
        return payin;
    }

    public getPayoutURL(){
        const payout = this.backofficeURL + this.payoutsURL;
        return payout;
    }

    public getUsersURL(){
        const users = this.backofficeURL + this.usersURL;
        return users;
    }

    public getLoginURL(){
        const login = this.backofficeURL + this.loginURL;
        return login;
    }

    public getMerchantsURL(){
        const merchants = this.backofficeURL + this.merchantsURL;
        return merchants;
    }

    public getMerchantCodesURL(){
        const merchantcodes = this.backofficeURL + this.merchantCodeNamesURL;
        return merchantcodes;
    }

    public getCurrentRatesURL(){
        const currentrate = this.merchantCurrentRateURL;
        return currentrate;
    }

    public handleError(error:any){
        this.router.navigate(['/login'],{queryParams: {err:'yes'}});
    }

    public getSettlementResponse(url:string,page:number,size:number):Observable <any>{
        const params = new HttpParams().set('merchantId', "1").set('page', page.toString()).set('size', size.toString());
        return new Observable(observer => { this.http.get(url,{ params}).map(
          ( data: response) =>  data)
        .subscribe(data => {
          this.totalCount = data.totalItems;
          observer.next(data);
          observer.complete();
         });
        });
    }

    public isAdmin(){
        const roleid = localStorage.getItem("roleid");
        if (roleid == '1' ) {
           return true;
        }
        else{
            return false;
        }
    }

    public refreshPage() {
        location.reload()
    }
}