import { Component,OnInit } from '@angular/core';
import { getOriginalError } from '@angular/core/src/errors';
import { local } from 'angular-io-datepicker';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit{
    isActive = false;
    showMenu = '';
    role ;

    eventCalled() {
        this.isActive = !this.isActive;
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    
    getRole(){
        this.role = localStorage.getItem("roleid");
    }
    ngOnInit() : void {       
        this.getRole();
    }
}