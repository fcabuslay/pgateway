import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from './user';
import { Balance } from './balance';
import { AuthService } from '../../../services/auth.service'


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],

})
export class HeaderComponent implements OnInit {
    user: any;
    loggedinUser : User;
    pushRightClass: string = 'push-right';
    admin: boolean;
    runningb: Balance;

    constructor(private translate: TranslateService,private authService: AuthService, public router: Router) {
        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.getLoggedInUser();
        this.getBalance();
    }

    getLoggedInUser(): void {
        this.authService.getProfileResponse().subscribe(loggedinUser => this.loggedinUser = loggedinUser,
            error => {
            this.authService.handleError(error);
          });

        if (localStorage.getItem("username") === null) {
            this.router.navigate(['/login']);
        }
        else {
            this.user = localStorage.getItem("username");
        }
    }

    isAdmin(): boolean{
        this.admin =  this.authService.isAdmin()
        return this.admin
    }

    getBalance(): void {
        if (!this.isAdmin()){
        this.authService.getBalance().subscribe(            
            res =>  this.runningb = res.balances,
            (err) => {
              this.authService.handleError(err);
            }
          );
        }
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        localStorage.clear();
    }

    changeLang(language: string) {
        this.translate.use(language);
    }
}
