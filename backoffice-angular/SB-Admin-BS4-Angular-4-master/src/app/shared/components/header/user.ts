export interface User {
  id: number;
  username: string;     
  password: string;
  email: string;
  enabled: string;
  firstName: string; 
  lastName: string; 
  merchant: number[];
  roleId: number;
  roleName: string;
  
}