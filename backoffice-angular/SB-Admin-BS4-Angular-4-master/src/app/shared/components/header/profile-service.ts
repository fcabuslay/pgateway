import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http } from '@angular/http';
import { BackofficeService } from '../../../shared/services/backoffice.service';
import 'rxjs/Rx';
import { User } from './user';
import { Router } from '@angular/router';

@Injectable()
export class ProfileService {
    private url: string;
    //private userURL = 'http://localhost:8882/backoffice/api/users/1'; 


    constructor(
    private http: HttpClient,
    private router: Router,
    private bs: BackofficeService) {
        this.generateURL();
    }

    getLoggedInUser(): Observable<User>{
       
        return this.http.get<User>(this.url).catch((err) => {
                
            // Do messaging and error handling here
          //  this.router.navigate(['login']);
            return Observable.throw(err)
        })
    }

    getUserObservable(): Observable<User> {
        //const url = `${this.url}/${id}`;
        return this.http.get<User>(this.url);
      }

    private handleError(error: any): Observable<any> {
        console.log('An error occurred', error); // for demo purposes only
        this.router.navigate(['login']);
        return Observable.throw(error);
    }

    generateURL(){
        this.url = this.bs.getUsersURL()+"/profile";
      }
}