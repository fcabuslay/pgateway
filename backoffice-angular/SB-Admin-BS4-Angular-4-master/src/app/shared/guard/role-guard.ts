import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class RoleGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        const expectedRole = route.data.expectedRole;

        if (localStorage.getItem('isLoggedin') && localStorage.getItem('role')==expectedRole) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}