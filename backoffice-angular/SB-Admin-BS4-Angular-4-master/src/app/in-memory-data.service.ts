import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const users = [
        { id: 11, username: 'John', password: '1234', email: 'John@doe.com', 'firstName':'John', 'lastName': 'doe', merchant: [0,1,2], roleId: 1},
        { id: 12, username: 'Jane', password: '1234', email: 'Jane@doe.com', 'firstName':'Jane', 'lastName': 'doe', merchant: [0,1,2], roleId: 2}
    ];
    return {users};
  }
}