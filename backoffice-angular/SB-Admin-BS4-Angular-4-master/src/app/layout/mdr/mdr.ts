export interface MDR {
    id: number;
    MerchantName: string;
    currency: string;
    transtype: string;
    channel: string;
    rate: number;
    startdate: string;
    createdby: string;
    remarks: string;
  }
  