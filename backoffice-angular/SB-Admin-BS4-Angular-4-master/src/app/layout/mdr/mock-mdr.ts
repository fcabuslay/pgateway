import { MDR } from './mdr';

export const mdr: MDR[] = [
  { id: 11, MerchantName: 'MerchantCNY', currency: 'CNY', transtype: 'Payin', channel: 'BANK_TRANSFER', rate: 0.8, startdate: '2017-10-23T13:54:00', createdby: 'jdoe', remarks: 'initial rate'},
  { id: 11, MerchantName: 'MerchantCNY', currency: 'CNY', transtype: 'Payin', channel: 'BANK_TRANSFER', rate: 0.8, startdate: '2017-10-23T13:54:00', createdby: 'jdoe', remarks: 'initial rate'},
  { id: 11, MerchantName: 'MerchantCNY', currency: 'CNY', transtype: 'Payin', channel: 'BANK_TRANSFER', rate: 0.8, startdate: '2017-10-23T13:54:00', createdby: 'jdoe', remarks: 'initial rate'},
  /*
    id: number;
    MerchantName: string;
    currency: string;
    transtype: string;
    channel: number;
    rate: number;
    startdate: number;
    createdby: string;
    remarks: string;
   */
];