import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { MDRRoutingModule } from './mdr-routing.module';
import { MDRComponent } from './mdr.component';
import { PageHeaderModule } from './../../shared';
import { MDRService } from './mdr.service';
import { DatePickerModule } from 'angular-io-datepicker';
import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';



@NgModule({
    imports: [
        CommonModule,
        MDRRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        OverlayModule,
        DatePickerModule,
    ],
   declarations: [MDRComponent],
   providers: [ MDRService ],
   bootstrap: [ MDRComponent ]
})
export class MDRModule { }
