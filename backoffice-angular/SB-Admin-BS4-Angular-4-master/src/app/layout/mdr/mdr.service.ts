import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import { MDR } from './mdr';
import { mdr } from './mock-mdr';

@Injectable()
export class MDRService {

  private mdrURL = 'https://05b1cc36-cf7a-4400-9847-152507bdaa90.mock.pstmn.io/payins';  // URL to web api
  // private headers = new Headers({'Content-Type': 'application/json'});

 constructor(private http: Http) { }

   getMDR(): Promise<MDR[]> {
   return this.http.get(this.mdrURL)
              .toPromise()
              .then(res => res.json() as MDR[])
              .catch(this.handleError);
 }

 private handleError(error: any): Promise<any> {
   console.error('An error occurred', error); // for demo purposes only
   return Promise.reject(error.message || error);
 }

  getMockHeroes(): Promise<MDR[]> {
    return Promise.resolve(mdr);
  }

}
