import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MDRComponent } from './mdr.component';

const routes: Routes = [
    { path: '', component: MDRComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MDRRoutingModule { }
