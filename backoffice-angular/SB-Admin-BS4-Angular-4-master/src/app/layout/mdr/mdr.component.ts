import { Component, OnInit } from '@angular/core';
import { MDR } from './mdr';
import { routerTransition } from '../../router.animations';
import { MDRService } from './mdr.service';


@Component({
    selector: 'app-mdr',
    templateUrl: './mdr.component.html',
    styleUrls: ['./mdr.component.scss'],
    providers: [MDRService],
    animations: [routerTransition()]
})
export class MDRComponent implements OnInit {
  mdr: MDR[];

  constructor(private mdrs:MDRService) { }

  getmdr(): void {
    this.mdrs.getMDR().then(mdr => this.mdr = mdr);
  }

  getMockmdr(): void {
    this.mdrs.getMockHeroes().then(mdr => this.mdr = mdr);
  }

  ngOnInit(): void {
    this.getMockmdr();
  }
}
