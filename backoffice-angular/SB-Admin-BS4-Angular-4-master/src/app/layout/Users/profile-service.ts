import { Injectable,OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import { User } from './user';
import { Router } from '@angular/router';
import { BackofficeService } from '../../shared/services/backoffice.service';

@Injectable()
export class ProfileService implements OnInit{

    //private userURL = 'http://localhost:8882/backoffice/api/users/1'; 
    url:string;

    constructor(
    private http: HttpClient,
    private router: Router,
    private bs: BackofficeService,
    ) {
    this.generateURL();
    }

    generateURL(){
        const id = localStorage.getItem("id");
        this.url = this.bs.getUsersURL()+'/'+id;
    }

    getLoggedInUser(): Observable<User>{
       
        return this.http.get<User>(this.url,{ withCredentials: false}).catch((err) => { 
        //return this.http.get<User>(this.url).catch((err) => {   
            // Do messaging and error handling here
           // this.router.navigate(['login']);
            return Observable.throw(err)
        })
    }

    private handleError(error: any): Observable<any> {
        console.log('An error occurred', error); // for demo purposes only
        this.router.navigate(['login']);
        return Observable.throw(error);
    }
    ngOnInit(){

    }
}