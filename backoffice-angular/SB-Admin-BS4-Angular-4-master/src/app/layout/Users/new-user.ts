export class NewUser {
    username: string;     
    password: string;
    email: string;
    firstName: string; 
    lastName: string; 
    merchantIds: number[];
    roleId: number;
    constructor() { 
    }
  }
  
  /*
  "username": "admin",
  "password": "12345678",
  "email": "user@integration.com",
  "firstName": "Paul",
  "lastName": "Gasol",
  "merchant": [ 200, 201 ],
  "roleId": 550
  */