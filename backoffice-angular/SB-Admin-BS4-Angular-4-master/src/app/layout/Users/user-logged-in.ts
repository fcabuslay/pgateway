import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { routerTransition } from '../../router.animations';
import { UserService } from './user.service';
import { NgbModal,ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

