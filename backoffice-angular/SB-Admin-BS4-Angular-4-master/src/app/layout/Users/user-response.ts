import { User } from './user';
export class response {
    pageNum: number;
    itemCount: number;
    totalItems: number;
    totalPages: number;
    items: User[];
  }