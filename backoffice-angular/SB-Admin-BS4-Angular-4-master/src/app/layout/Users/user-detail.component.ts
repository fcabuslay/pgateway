import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { User } from './user';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service'

@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: [ './user-detail.component.scss' ]
  })
  export class UserDetailComponent implements OnInit {
    user: User;
    UserForm: FormGroup;
    post:any;
    username:string;
    password:string;
    email:string;
    firstname:string;
    lastname:string;
    merchantids:number;
    roleid:number;

    constructor(
      private route: ActivatedRoute,
      private location: Location,
      private authService : AuthService,
    ) {
    }

    ngOnInit(): void {
      this.getUser();
      this.UserForm = new FormGroup({ 
        'username':  new FormControl({disabled:true}),
        'email': new FormControl(),
        'firstname': new FormControl(),
        'lastname': new FormControl(),
        'roleid': new FormControl(),
      });
    }
   
    getUser(): void {
      const id = +this.route.snapshot.paramMap.get('id');
      this.authService.getUser(id)
        .subscribe(user => this.user = user);
    }
    
    editUser(post):void{
      const id = +this.route.snapshot.paramMap.get('id');
      this.firstname = post.firstname;
      this.lastname = post.lastname;
      this.email = post.email;
      this.roleid = post.roleid;
      this.authService.editUser(id,this.firstname,this.lastname,this.email,this.roleid).subscribe(res => {
        this.authService.notify("User successfully updated","success")
        this.getUser();
      },
        err => {
          this.authService.handleError(err);
        });
    }

    goBack(): void {
      this.location.back();
    }

    disable(): void {
      const id = +this.route.snapshot.paramMap.get('id');
      this.authService.disableUser(id);
    }

    enable(): void {
      const id = +this.route.snapshot.paramMap.get('id');
      this.authService.enableUser(id);
    }
  }