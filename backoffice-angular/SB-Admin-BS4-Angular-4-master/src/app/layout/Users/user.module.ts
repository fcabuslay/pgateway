import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { PageHeaderModule } from './../../shared';
import { UserService } from './user.service';
//import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JsessionIDInterceptor } from '../../shared/services/jsession.interceptor';


@NgModule({
    imports: [
        CommonModule,
        UserRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        Ng2SearchPipeModule,
        Ng2OrderModule,
        NgxPaginationModule
        //OverlayModule,
    ],
   declarations: [UserComponent],
   providers: [ UserService ,{
    provide: HTTP_INTERCEPTORS,
    useClass: JsessionIDInterceptor ,
    multi: true
  }],
   bootstrap: [ UserComponent ]
})
export class UserModule { }
