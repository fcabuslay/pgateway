import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { NgbModal,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MerchantCode } from '../merchants/merchantcode-response';
import { AuthService } from '../../services/auth.service'
import { CONFIG } from '../../config/config'

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  user: User[];
  AddUserForm: FormGroup;
  post:any;
  modalRef: NgbModalRef;
  username:string;
  password:string;
  email:string;
  firstname:string;
  lastname:string;
  merchantids:number;
  merchantcodes:string;
  roleid:number;
  role:string;
  merchantcode: MerchantCode[];
  map = new Map<string, string>(); 
  /*
  "username": "user1",
  "password": "12345678",
  "email": "user@integration.com",
  "firstName": "Paul",
  "lastName": "Gasol",
  "merchantIds": ["200","201"],
  "roleId": 550
  */

  //paginate
  key: string = 'createdBy'; //set default
  p: number = 1;
  size: number = 5;
  reverse: boolean = false;
  clicked: boolean = false;
  total:  number ;
  loading: boolean;
  result: any;
  private url:string;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private authService: AuthService

  ) { 

    this.AddUserForm = fb.group({
      'username' : '',
      'password' : '',
      'email' : '',
      'firstname':'',
      'lastname':'',
      'merchantids':'',
      'role':'',
      'merchantcodes':'',
    });
  }
  getUsers(page: number): void {
    this.p = page
    this.authService.getResponse(`${CONFIG.USERS_URL}`, page, 10).subscribe(data => {
      this.user = data.items,
      this.total = data.totalItems
    }),
      (err) => {
        this.authService.handleError(err)
      }
  }

  getMerchantCodes() {
    this.authService.getMerchantCode().subscribe(
      data => this.merchantcode = data,
      (err) => {
        this.authService.handleError(err);
      }
    );
  }

  open(content) {
    this.modalRef = this.modalService.open(content,{ size: 'lg' });
  }

  addPost(post) {
    this.merchantcodes = post.merchantcodes;
    this.username = post.username;
    this.password = post.password;
    this.email = post.email;
    this.firstname = post.firstname;
    this.lastname = post.lastname;
    this.merchantcodes = post.merchantcodes;
    this.role = post.role;
    this.modalRef.close();
    this.authService.createUser(this.username,this.password,this.email ,this.firstname ,this.lastname,this.merchantcodes,this.role).subscribe(res => {
      this.authService.notify("User successfully created","success")
      this.getUsers(1);
    },
      err => {
        this.authService.handleError(err);
      });
  }
  

  ngOnInit(): void {
    this.getUsers(1);
    this.getMerchantCodes();
  }

}
