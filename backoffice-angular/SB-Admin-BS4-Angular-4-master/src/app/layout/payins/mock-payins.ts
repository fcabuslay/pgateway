/*

import { Payin } from './payin';

export const PAYINS: Payin[] = [
  { id: 11, ponum: 'PG-123', merchant: 'MERCHANT1', cartid: 'M-123', paytype: 'Bank Transfer', currency: 'CNY', orderamt: 10.00, transamt: 9.91, transcharge: 0.9, status: 'Completed', startdate: '2017-10-23T13:54:00', enddate: '2017-10-23T13:57:20'},
  { id: 11, ponum: 'PG-123', merchant: 'MERCHANT2',cartid: 'M-123', paytype: 'Bank Transfer', currency: 'CNY', orderamt: 10.00, transamt: 9.91, transcharge: 0.9, status: 'Pending', startdate: '2017-10-22T03:54:00', enddate: '2017-10-22T03:57:20'},
  { id: 11, ponum: 'PG-123', merchant: 'MERCHANT3',cartid: 'M-123', paytype: 'Bank Transfer', currency: 'CNY', orderamt: 10.00, transamt: 9.91, transcharge: 0.9, status: 'Failed', startdate: '2017-10-21T08:54:00', enddate: '2017-10-21T08:57:20'}
  /*
     id: string;
  ponum: string;
  cartid: string;
  paytype: string;
  currency: string;
  orderamt: number;
  transamt: number;
  transcharge: number;
  status: string;
  startdate: Date;
  enddate: Date;
  firstname: string;
  lastname: string;

  PO Bill Number  Cart ID Payment Type  Currency  Order Amount  Transaction Amount  Transaction Charge  Status  Created Time  Completed Time  Action
PG-123  M-123 Bank Transfer CNY 10.00 9.91  0.9 Completed 2017-10-23 08:27:54 2017-10-23 08:54:01
];

*/