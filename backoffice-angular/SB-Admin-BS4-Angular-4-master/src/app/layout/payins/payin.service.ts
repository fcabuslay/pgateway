import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import { Payin } from './payin';
import { Observable } from 'rxjs/Observable';
//import { PAYINS } from './mock-payins';
import { Merchant} from '../merchants/merchant';
import { MERCHANTS } from '../merchants/mock-merchant';
import { HttpClient,HttpHeaders,HttpParams } from '@angular/common/http';
import { BackofficeService } from '../../shared/services/backoffice.service';


@Injectable()
export class PayinService {

  //private payinsURL = 'http://localhost:8882/backoffice/api/transactions/payins';  // URL to web api
  //private payinsURL = 'http://localhost:8881/api/transactions/payins';  // URL to web api
  private url:string;
  

 constructor(private http: HttpClient,private bs: BackofficeService){
  this.generateURL();
 }

generateURL(){
  this.url = this.bs.getPayinURL();
}

getData() {
  const params = new HttpParams().set('merchantId', "1").set('page', "0").set('size', "5");
  this.http.get<Payin>(this.url,{params}).subscribe(res => {
    //console.log(res);;
  });
}


getPayinsObservable(): Observable<Payin[]>{
  //const params = new HttpParams().set('merchantId', "1").set('page', "0").set('size', "5");
  const params = new HttpParams().set('page', "0").set('size', "5");
  return this.http.get<Payin>(this.url,{params}).map(
    ( res: any) => res.items
  ).catch(this.handleError)
}

getPayinByID(id:number): Observable<Payin[]>{
  const params = new HttpParams().set('merchantId', "1").set('page', "0").set('size', "5").set('id',id.toString());
  return this.http.get<Payin>(this.url,{params}).catch(this.handleError)
}

  getDataPromise():Promise<Payin[]> {
  return this.http.get(this.url)
  .toPromise()
  .then(res => res as Payin[])
  .catch(this.handleError);
  }

  getData2() {
    return this.http.get<Payin[]>(this.url)
    .subscribe
    (res => res as Payin[])
    }
  

  /*
   getPayins(): Promise<Payin[]> {
   return this.http.get(this.payinsURL)
              .toPromise()
              .then(res => res.json() as Payin[])
              .catch(this.handleError);
            
 }
*/
 private handleError(error: any): Promise<any> {
   console.error('An error occurred', error); // for demo purposes only
   return Promise.reject(error.message || error);
 }

 /*
  getMockHeroes(): Promise<Payin[]> {
    return Promise.resolve(PAYINS);
  }
*/
  getMockMerchants(): Promise<Merchant[]>{
    return Promise.resolve(MERCHANTS);
  }

}
