export interface PayinSummary {
    merchantid: number; 
    merchantcode: string; 
    transactiontype: string; 
    breakdowns: any[]; 
}