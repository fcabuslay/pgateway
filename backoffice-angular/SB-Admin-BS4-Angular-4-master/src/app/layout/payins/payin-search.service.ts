import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import { Payin } from './payin';
import { Observable } from 'rxjs/Observable';
//import { PAYINS } from './mock-payins';
import { Merchant} from '../merchants/merchant';
import { MERCHANTS } from '../merchants/mock-merchant';
import { HttpClient,HttpHeaders,HttpParams } from '@angular/common/http';
import { BackofficeService } from '../../shared/services/backoffice.service';


@Injectable()
export class PayinSearchService {

  private payinsURL = 'http://localhost:8882/backoffice/api/transactions/payins/search?page=0&size=5';  // URL to web api
  private url:string;
 value1 : string;

 constructor(private http: HttpClient,private bs: BackofficeService) {
  
  }
  generateURL(){
    this.url = this.bs.getPayinURL()+'/search?page=0&size=2';
  }
 getData() {
  const params = new HttpParams().set('page', "0").set('size', "5");
  this.http.get<Payin>(this.payinsURL,{params}).subscribe(res => {
  });
}
getPayinFromSearch(result: any): Observable<Payin[]>{
    const headers = new HttpHeaders().set("Content-Type","application/x-www-form-urlencoded");

    return this.http.post<Payin>(this.url, {
        headers,
        createdDateFrom: result['value1'],
        createdDateTo: result['value2'],
        currencies: null,
        merchantIds:result['selectedmerchant'],
        methods:null,
        statuses:null,
      },
    ).catch(this.handleError)
  }

  getPayinFromSearch2(result: any): Observable<Payin[]>{
    const headers = new HttpHeaders().set("Content-Type","application/x-www-form-urlencoded");
    const params = new HttpParams()
    .set('createdDateFrom',result['createdDateFrom'])
    .set('createdDateTo',result['createdDateTo'])
    .set('id',result['id'])
    .set('cartid',result['cartid'])
    .set('currencies',result['currency'])
    .set('merchantIds',result['selectedmerchant'])
    .set('method',result['method'])
    .set('status',result['status']);
    return this.http.post<Payin[]>(this.url, params,{
            headers: headers
          }
    );
  }

  

getPayinsObservable(): Observable<Payin[]>{
  const params = new HttpParams().set('page', "0").set('size', "5");
  return this.http.get<Payin>(this.url,{params}).catch(this.handleError)
}

getPayinByID(id:number): Observable<Payin[]>{
  const params = new HttpParams().set('merchantId', "1").set('page', "0").set('size', "5").set('id',id.toString());
  return this.http.get<Payin>(this.payinsURL,{params}).catch(this.handleError)
}

  getDataPromise():Promise<Payin[]> {
  return this.http.get(this.payinsURL)
  .toPromise()
  .then(res => res as Payin[])
  .catch(this.handleError);
  }

  getData2() {
    return this.http.get<Payin[]>(this.payinsURL)
    .subscribe
    (res => res as Payin[])
    }
  

  /*
   getPayins(): Promise<Payin[]> {
   return this.http.get(this.payinsURL)
              .toPromise()
              .then(res => res.json() as Payin[])
              .catch(this.handleError);
            
 }
*/
 private handleError(error: any): Promise<any> {
   console.error('An error occurred', error); // for demo purposes only
   return Promise.reject(error.message || error);
 }

 /*
  getMockHeroes(): Promise<Payin[]> {
    return Promise.resolve(PAYINS);
  }
*/
  getMockMerchants(): Promise<Merchant[]>{
    return Promise.resolve(MERCHANTS);
  }

  ngOnInit(): void {
  this.generateURL();
  }
}
