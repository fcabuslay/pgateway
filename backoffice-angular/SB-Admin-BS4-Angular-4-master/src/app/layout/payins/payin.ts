export interface Payin {
  id: number; //gateway transaction ID 
  transid: string; //gateway transaction ID - still missing
  merchantId: string; // ID number of merchant
  merchantTransId: string; //Merchant Transaction ID
  method: string; //BANK_TRANSFER, WECHAT, ETC
  currency: string; //CNY
  amount: number; //order amount
  status: string; //PENDING, COMPLETED, etc
  orderDate: string; //Time order was generated
  enddate: string; //Completed / Failed Date
}

/*
{
    "payins": [
        {
            "id": 3,
            "apiVersion": "1.0",
            "merchantId": 1,
            "merchantTransId": "5011",
            "amount": 15,
            "currency": "CNY",
            "payinCode": "CNICBC",
            "method": "BANK_TRANSFER",
            "status": "PENDING",
            "payinDetails": {
                "orderDate": "2017-11-15 14:25:59",
                "callbackUrl": "http://merchant1.com/callbacks",
                "returnUrl": "http://merchant1.com/return",
                "product": "Accessories",
                "param1": "Param 1",
                "param2": "Param 2",
                "param3": "Param 3"
            },
            "methodDetails": {
                "bank": "CNICBC",
                "method": "BANK_TRANSFER"
            }
        },
        {
            "id": 1,
            "apiVersion": "1.0",
            "merchantId": 1,
            "merchantTransId": "5008",
            "amount": 15,
            "currency": "CNY",
            "payinCode": "CNICBC",
            "method": "BANK_TRANSFER",
            "status": "PENDING",
            "payinDetails": {
                "orderDate": "2017-11-15 14:25:59",
                "callbackUrl": "http://merchant1.com/callbacks",
                "returnUrl": "http://merchant1.com/return",
                "product": "Accessories",
                "param1": "Param 1",
                "param2": "Param 2",
                "param3": "Param 3"
            },
            "methodDetails": {
                "bank": "CNICBC",
                "method": "BANK_TRANSFER"
            }
        }
    ]
}
*/
