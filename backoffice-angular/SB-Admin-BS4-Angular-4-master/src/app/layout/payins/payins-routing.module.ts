import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PayinsComponent } from './payins.component';
import { PayinSearchComponent } from './payin-search.component';
const routes: Routes = [
    { path: '', component: PayinsComponent },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PayinsRoutingModule { }
