import { Component, OnInit } from '@angular/core';
import { Payin } from './payin';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MerchantCode } from '../merchants/merchantcode-response';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { AuthService } from '../../services/auth.service'
import { CONFIG } from '../../config/config'

@Component({
  selector: 'app-payin',
  templateUrl: './payins.component.html',
  styleUrls: ['./payins.component.scss'],
})

export class PayinsComponent implements OnInit {
  SearchForm: FormGroup;
  post: any;    //form group   
  closeResult: string;
  merchantcode: MerchantCode[];
  createdDateFrom: string;
  createdDateTo: string;
  status: string;
  currency: string;
  method: string;
  selectedmerchant: string;
  id: string;
  cartid: string;
  modalRef: NgbModalRef;
  //search, paginate, etc below:
  key: string = 'id'; //set default
  p: number = 1;
  reverse: boolean = true;
  size: number = 5;
  total: number;
  result: any;
  payins: Payin[];
  admin: Boolean;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService

  ) {
    this.SearchForm = fb.group({
      'value1': '',
      'value2': '',
      'selectedmerchant': '',
      'status': '',
      'pobill': '',
      'cartid': '',
      'currency': '',
      'method': '',
    });
  }

  ngOnInit(): void {
    this.getPayins(1)
    this.getMerchantCodes()
    this.isAdmin()
  }

  isAdmin(){
    this.admin =  this.authService.isAdmin();
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
  }

  addPost(post) {
    this.createdDateFrom = this.transform(post.value1);
    this.createdDateTo = this.transform(post.value2);
    this.selectedmerchant = post.selectedmerchant;
    this.id = post.pobill;
    this.cartid = post.cartid;
    this.currency = post.currency;
    this.method = post.method;
    this.status = post.status;
    this.router.navigate(['/payins/search', { createdDateFrom: this.createdDateFrom, createdDateTo: this.createdDateTo, selectedmerchant: this.selectedmerchant, id: this.id, cartid: this.cartid, currency: this.currency, method: this.method, status: this.status }]);
    this.modalRef.close();
  }

  transform(value: string) {
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(value, 'yyyy-MM-dd H:mm:ss');
    return value;
  }

  getPayins(page: number): void {
    this.p = page
    this.authService.getResponse(`${CONFIG.PAYIN_URL}`, page, 10).subscribe(data => {
      this.payins = data.items,
      this.total = data.totalItems
    }),
      (err) => {
        this.authService.handleError(err)
      }
  }

  getMerchantCodes() {
    this.authService.getMerchantCode().subscribe(
      data => this.merchantcode = data,
      (err) => { 
        this.authService.handleError(err);
      }
    );
  }

}
