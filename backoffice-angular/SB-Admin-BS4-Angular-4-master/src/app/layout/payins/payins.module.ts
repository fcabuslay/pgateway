import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { PayinsRoutingModule } from './payins-routing.module';
import { PayinsComponent } from './payins.component';
import { PageHeaderModule } from './../../shared';
import { PayinService } from './payin.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule } from 'primeng/primeng';
import { AuthService } from '../../services/auth.service'
import { NotifyService } from '../../services/notify.service'

//import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import { DateTimePickerModule } from 'ng-pick-datetime';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import {A2Edatetimepicker} from 'ng2-eonasdan-datetimepicker';



@NgModule({
    imports: [
        CommonModule,
        //A2Edatetimepicker,
        PayinsRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        CalendarModule,
        NgbModule,
        Ng2SearchPipeModule,
        Ng2OrderModule,
        NgxPaginationModule
       // OverlayModule,
        //DatePickerModule,
      // DateTimePickerModule,
   //     NgbModule,
    ],
   declarations: [PayinsComponent],
   providers: [ PayinService,AuthService,NotifyService ],
   bootstrap: [ PayinsComponent ]
})
export class PayinsModule { }