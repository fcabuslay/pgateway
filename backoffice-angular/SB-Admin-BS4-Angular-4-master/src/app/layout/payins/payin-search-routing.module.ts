import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PayinSearchComponent } from './payin-search.component';
const routes: Routes = [
    { path: '', component: PayinSearchComponent },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PayinSearchRoutingModule { }
