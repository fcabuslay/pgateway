import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl ,FormBuilder,FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service';
import { MerchantCode } from '../merchants/merchantcode-response';
import { MidCode } from '../mid-management/midcode'
import { Mid } from './mid';

@Component({
    selector: 'app-mid-detail',
    templateUrl: './mid-detail.component.html',
    styleUrls: [ './mid-detail.component.scss' ]
  })

  export class MidDetailComponent implements OnInit {
    mid: Mid;
    MidForm: FormGroup;
    modalmid: FormGroup;
    control: FormControl;
    post:any;
    merchantcode: MerchantCode[];
    midcodes: MidCode[];
    modalRef: NgbModalRef;
    modalRef2: NgbModalRef;
    merchant: string;
    params: FormArray;
    constructor(
        private route: ActivatedRoute,
        private location: Location,
        private router: Router,
        private modalService: NgbModal,
        private modalService2: NgbModal,
        private authService: AuthService,
        private fb: FormBuilder,
    ) {
      this.MidForm =fb.group({
        'id': '',
        'code': '',
        'transactionType': '',
        'method': '',
        'currency': '',
        'midConfigCode': '',
        'weight': '',
        'enabled': '',
        'minamount': '',
        'maxamount': '',
      });

     
      this.modalmid = fb.group({
        'midcode': '',
        'currency': '',
        'minamount': '',
        'maxamount': '',
        params: this.fb.array([ this.createItem() ])
      });

    }

    //open(content,merchant:string,mid:string) {
      open(content)  {
        //this.process(merchant).subscribe(status => this.merchant = status);
        //this.RoutingModal.setControl.u['merchantCode'].updateValueAndValidity("d")
        //this.RoutingModal.controls['midConfigCode'].setValue(mid);
        this.modalRef = this.modalService.open(content);
      }

      openfields(content) {
        this.modalRef2 = this.modalService2.open(content);
      }


    ngOnInit(): void {
        this.getMid()
    }

    createItem(): FormGroup {
      return this.fb.group({
        CALLBACK_URL: '',
        MID: '',
        REMARKS: ''
      });
    }

    getMid() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.authService.getMid(id).subscribe(
          data => this.mid = data )
    }

    getMidDetail(): void {
      const id = +this.route.snapshot.paramMap.get('id');
      this.router.navigate(['/midmanagement/'+id+'/details']);
    }

    updateMid(post){
        const id = +this.route.snapshot.paramMap.get('id');
        this.modalRef.close()
        this.authService.updateMid(id,post).subscribe(res => {
          this.authService.notify("Mid successfully updated","success")
          this.getMid()
        },
          err => {
            this.authService.handleError(err);
          });  
    }

    enableMid(){
      const id = +this.route.snapshot.paramMap.get('id');
     // return this.authService.enableMid(id)
     this.authService.enableMid2(id).subscribe( res => { 
      this.getMid()
      this.authService.notify("Mid successfully enabled","success")
      err => {
        this.authService.handleError(err);
      }});
    }

    disableMid(){
      const id = +this.route.snapshot.paramMap.get('id');
      //return this.authService.disableMid(id)
      this.authService.disableMid2(id).subscribe( res => { 
        this.getMid()
        this.authService.notify("Mid successfully disabled","success")
        err => {
          this.authService.handleError(err);
        }});
    }

    archiveMid(){
      const id = +this.route.snapshot.paramMap.get('id');
      //return this.authService.archiveMid(id)
      return this.authService.archiveMid2(id).subscribe( res => { 
        this.router.navigate(['/midmanagement']);
        this.authService.notify("Mid successfully archived","success")
        err => {
          this.authService.handleError(err);
        }});
    }

    UpdateBalance(post) {
      const id = +this.route.snapshot.paramMap.get('id');
      return this.authService.UpdateBalance(id,post)
    }
    
    goBack(): void {
      this.location.back();
    }

    getMerchantCodes() {
        this.authService.getMerchantCode().subscribe(
          data => this.merchantcode = data,
          (err) => {
            this.authService.handleError(err);
          }
        );
      }


    
      getMidCodes() {
        this.authService.getMidCodes().subscribe( data => this.midcodes = data ,
        (err) => {
          this.authService.handleError(err);
        }
      );
      }

  }