import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl ,FormBuilder,FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service';
import { MerchantCode } from '../merchants/merchantcode-response';
import { MidCode } from '../mid-management/midcode'
import { Mid } from './mid';
import { MidField } from './mid-field';

@Component({
    selector: 'app-mid-fields',
    templateUrl: './mid-fields.component.html',
    styleUrls: [ './mid-fields.component.scss' ]
  })

  export class MidFieldsComponent implements OnInit {
    mid: Mid;
    MidForm: FormGroup;
    modalmid: FormGroup;
    control: FormControl;
    post:any;
    merchantcode: MerchantCode[];
    midcodes: MidCode[];
    midfield: MidField;
    modalRef: NgbModalRef;
    merchant: string;
    params: FormArray;
    constructor(
        private route: ActivatedRoute,
        private location: Location,
        private router: Router,
        private modalService: NgbModal,
        private modalService2: NgbModal,
        private authService: AuthService,
        private fb: FormBuilder,
    ) {
      this.MidForm =fb.group({
        'id': '',
        'code': '',
        'pspconfigcode': '',
        'merchantid': '',
        'callbackurl': '',
        'requesturl': '',
        'sign': '',
        'remarks':'',
        'landingurl': '',
        'privatekey': ''
      });

      this.modalmid = fb.group({
        'merchantid': '',
        'callbackurl': '',
        'requesturl': '',
        'sign': '',
        'remarks': '',
        'landingurl': '',
        'privatekey': ''
      });

    }

    //open(content,merchant:string,mid:string) {
      open(content)  {
        //this.process(merchant).subscribe(status => this.merchant = status);
        //this.RoutingModal.setControl.u['merchantCode'].updateValueAndValidity("d")
        //this.RoutingModal.controls['midConfigCode'].setValue(mid);
        this.modalRef = this.modalService.open(content);
      }

      updateMid(post){
        const id = +this.route.snapshot.paramMap.get('id');
        this.modalRef.close()
        this.authService.updateMidDetails(id,post).subscribe(res => {
          this.authService.notify("Mid details successfully updated","success")
          this.getMidField()
        },
          err => {
            this.authService.handleError(err);
          });  
    }

    ngOnInit(): void {
        this.getMidField()
    }

    getMidField() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.authService.getMidField(id).subscribe(
          data => this.midfield = data ,
          err => this.authService.handleError(err)
          );
    }

    
    goBack(): void {
      this.location.back();
    }


  }