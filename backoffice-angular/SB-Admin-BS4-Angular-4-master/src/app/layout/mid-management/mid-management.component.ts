import { Component, OnInit } from '@angular/core';
import { Mid } from './mid';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { AuthService } from '../../services/auth.service'
import { CONFIG } from '../../config/config'
import { PSPConfig } from './psp-config'

@Component({
  selector: 'app-mid-management',
  templateUrl: './mid-management.component.html',
  styleUrls: ['./mid-management.component.scss'],
})

export class MidManagementComponent implements OnInit {
  SearchForm: FormGroup;
  post: any;    //form group   
  closeResult: string;
  createdDateFrom: string;
  createdDateTo: string;
  transactiontype: string;
  currency: string;
  method: string;
  selectedmerchant: string;
  code: string;
  cartid: string;
  modalRef: NgbModalRef;
  //search, paginate, etc below:
  key: string = 'id'; //set default
  p: number = 1;
  reverse: boolean = true;
  size: number = 5;
  total: number;
  result: any;
  mids: Mid[];
  pspconfigs: PSPConfig[];
  

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService

  ) {
    this.SearchForm = fb.group({
      'code': '',
      'transactiontype': '',
      'method': '',
      'currency': '',
      'minamount': '',
      'maxamount': '',
      'pspconfig': '',
    });
  }

  ngOnInit(): void {
    this.getPayinMids(1)
    this.getPSPConfigs()
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
  }

  addPost(post) {
    this.modalRef.close();
    //this.code = post.code;
    //this.transactiontype = post.transactiontype;
    //this.method = post.method;
    //this.currency = post.currency;
    //this.minamount = post.minamount;
    //this.maxamount = post.maxamount;
    this.authService.processMid(post).subscribe(res => {
      this.authService.notify("Mid successfully created","success")
      this.getPayinMids(1)
    },
      err => {
        this.authService.handleError(err);
      });
  }
    /*
    this.authService.processMid2(post).subscribe( res => { 
      this.getPayinMids(1)
      this.getPSPConfigs()
      this.authService.notify("Mid successfully created","success")
      err => {
        this.authService.handleError(err);
      }});
      */
    
  

  

  transform(value: string) {
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(value, 'yyyy-MM-dd H:mm:ss');
    return value;
  }

  getPayinMids(page: number): void {
    this.p = page
    this.authService.getResponse(`${CONFIG.MID_CONFIG_URL}`, page, 10).subscribe(data => {
      this.mids = data.items,
      this.total = data.totalItems
    }),
      (err) => {
        this.authService.handleError(err)
      }
  }

  getPSPConfigs() {
    this.authService.getPSPConfigs().subscribe(
      data => this.pspconfigs = data,
      (err) => {
        this.authService.handleError(err);
      }
    );
  }

}
