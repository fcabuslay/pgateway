export interface Mid {
    id: number;
    code: string; 
    enabled: boolean;
    transactionType: string; 
    method: string; 
    limits:any[];
  }