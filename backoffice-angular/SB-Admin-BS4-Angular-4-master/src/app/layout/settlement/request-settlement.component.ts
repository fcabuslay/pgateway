import { Component, OnInit } from '@angular/core';
import { settlement } from './settlement';
import { routerTransition } from '../../router.animations';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../services/auth.service'
import { Location } from '@angular/common';

@Component({
  selector: 'app-requestsettlement',
  templateUrl: './request-settlement.component.html',
  styleUrls: ['./request-settlement.component.scss'],
  animations: [routerTransition()]
})
export class RequestSettlementComponent implements OnInit {

  settlements: settlement[];
  SettlementForm: FormGroup;
  post: any;
  amount: number;
  bankname: string;
  bankbranch: string;
  accountnumber: string;
  accountname: string;
  note: string;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private location: Location,
  ) {
    this.SettlementForm = this.fb.group({
      //amount:  [null, Validators.required,Validators.pattern('\d+(\.\d{1,2})?')],
      amount: [null, [Validators.required, Validators.pattern(/^[+-]?((\d+(\.\d*)?)|(\.\d+))$/)]],
      bankname: [null, Validators.required],
      bankbranch: [null, Validators.required],
      accountnumber: [null, Validators.required],
      accountname: [null, Validators.required],
      note: [null],
    });
  }

  isFieldValid(field: string) {
    return !this.SettlementForm.get(field).valid && this.SettlementForm.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }


  reset() {
    this.SettlementForm.reset();
  }

  goBack(): void {
    this.location.back();
  }

  createSettlement(post) {
    if (this.SettlementForm.valid) {
      this.amount = post.amount;
      this.bankname = post.bankname;
      this.bankbranch = post.bankbranch;
      this.accountnumber = post.accountnumber;
      this.accountname = post.accountname;
      this.note = post.note;
      this.authService.createSettlement(this.amount, this.bankname, this.bankbranch, this.accountnumber, this.accountname, this.note)
        .subscribe(res => {
          this.authService.notify("Settlement request sent", "success")
        },
          err => {
            this.authService.handleError(err);
          });
    }
    else {
      this.authService.validateAllFormFields(this.SettlementForm);
    }
  }

ngOnInit(): void {

}
}