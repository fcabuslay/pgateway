import { Component, OnInit } from '@angular/core';
import { settlement } from './settlement';
import { AuthService } from '../../services/auth.service'
import { CONFIG } from '../../config/config'

@Component({
  selector: 'app-settlement',
  templateUrl: './settlement.component.html',
  styleUrls: ['./settlement.component.scss'],
})

export class SettlementComponent implements OnInit {

  settlements: settlement[];
  key: string = 'id'; //set default
  p: number = 1;
  reverse: boolean = true;
  total: number;
  admin: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.getSettlements(1);
    this.isAdmin();
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  getSettlements(page: number): void {
    this.p = page
    this.authService.getResponse(`${CONFIG.SETTLEMENT_URL}`, page, 10).subscribe(data => {
      this.settlements = data.items,
        this.total = data.totalItems
    }),
      (err) => {
        this.authService.handleError(err)
      }
  }


  isAdmin() {
    this.admin = this.authService.isAdmin();
  }


}