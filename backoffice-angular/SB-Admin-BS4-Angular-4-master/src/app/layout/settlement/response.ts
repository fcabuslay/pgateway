import { settlement } from './settlement';
export class response {
    pageNum: number;
    itemCount: number;
    totalItems: number;
    totalPages: number;
    items: settlement[];
  }