import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettlementDetailComponent } from './settlement-detail.component';


const routes: Routes = [
    { path: '', component: SettlementDetailComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SettlementDetailRoutingModule { }
