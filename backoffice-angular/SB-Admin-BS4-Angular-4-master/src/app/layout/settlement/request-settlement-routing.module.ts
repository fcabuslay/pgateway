import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestSettlementComponent } from './request-settlement.component';

const routes: Routes = [
    { path: '', component: RequestSettlementComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RequestSettlementRoutingModule { }
