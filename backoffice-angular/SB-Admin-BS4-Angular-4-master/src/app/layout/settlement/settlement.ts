export class settlement {
    refcode: number;
    bankName: string;
    bankAccountNumber: string;
    bankAccountName: string;
    bankCode: string;
    bankBranch: string;
    currency: string;
    amount: number;
    status: string;
    createdTime: string;
    completedTime: string;
    note: string;
  }