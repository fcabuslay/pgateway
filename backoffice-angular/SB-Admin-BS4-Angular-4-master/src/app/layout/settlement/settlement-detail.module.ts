import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { SettlementDetailRoutingModule } from './settlement-detail-routing.module';
import { SettlementDetailComponent } from './settlement-detail.component';
import { PageHeaderModule } from './../../shared';
import { DatePickerModule } from 'angular-io-datepicker';
import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
    imports: [
        CommonModule,
        SettlementDetailRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        OverlayModule,
        DatePickerModule,
        NgbModule,
    ],
   declarations: [SettlementDetailComponent],
   providers: [ ],
   bootstrap: [ SettlementDetailComponent ]
})
export class SettlementDetailModule { }
