import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { RequestSettlementRoutingModule } from './request-settlement-routing.module';
import { RequestSettlementComponent } from './request-settlement.component';
import { PageHeaderModule } from './../../shared';
import { RequestSettlementService } from './request-settlement.service';
import { DatePickerModule } from 'angular-io-datepicker';
import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { FieldErrorDisplayComponent } from '../common/field-error-display.component';



@NgModule({
    imports: [
        CommonModule,
        RequestSettlementRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        OverlayModule,
        DatePickerModule,
        
    ],
   declarations: [RequestSettlementComponent,FieldErrorDisplayComponent],
   providers: [ RequestSettlementService ],
   bootstrap: [ RequestSettlementComponent ]
})
export class RequestSettlementModule { }
