import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { settlement } from './settlement';
import { SETTLEMENTS } from './mock-settlement';

@Injectable()
export class SettlementService {

  private settlementsURL = 'http://localhost:8882/backoffice/api/transactions/settlements';  // URL to web api
  // private headers = new Headers({'Content-Type': 'application/json'});
  //hardcode for now
  
  private userURL = 'http://localhost:8882/backoffice/api/users/8'
  //get merchants from user
  private merchantURL = 'http://localhost:8882/backoffice/api/merchants/1';

 constructor(private http: HttpClient) { }

  getsettlements() : Observable<settlement[]>{
    const params = new HttpParams().set('merchantId', "1").set('page', "0").set('size', "10");
    return this.http.get<settlement[]>(this.settlementsURL,{params}).map(
      ( res: any) => res.items
    ).catch(this.handleError)
  }
   

 /*
  getUsersObservable(): Observable<User[]> {
    const params = new HttpParams().set('page', "0").set('size', "10");
    return this.http.get<User[]>(this.userURL,{ params})
  }
 */

 private handleError(error: any): Promise<any> {
   console.error('An error occurred', error); // for demo purposes only
   return Promise.reject(error.message || error);
 }

  getMockSettlements(): Promise<settlement[]> {
    return Promise.resolve(SETTLEMENTS);
  }

}
