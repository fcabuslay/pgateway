import { settlement } from './settlement';

export const SETTLEMENTS: settlement[] = [
  { refcode: 7016336, bankName: 'Industrial and Commercial Bank of China', bankCode:'ICBC',bankAccountName: 'John Doe', bankAccountNumber: '47348489724398', bankBranch:'Shanghai',currency: 'CNY', amount: 50000, status: 'Pending', createdTime: '2017-10-23T13:54:00', completedTime: '2017-10-23T13:57:20',note:'Settle me now'},
  { refcode: 7016336, bankName: 'Industrial and Commercial Bank of China', bankCode:'ICBC',bankAccountName: 'John Doe', bankAccountNumber: '47348489724398', bankBranch:'Shanghai',currency: 'CNY', amount: 50000, status: 'Pending', createdTime: '2017-10-23T13:54:00', completedTime: '2017-10-23T13:57:20',note:'Settle me now'},
  { refcode: 7016336, bankName: 'Industrial and Commercial Bank of China', bankCode:'ICBC',bankAccountName: 'John Doe', bankAccountNumber: '47348489724398', bankBranch:'Shanghai',currency: 'CNY', amount: 50000, status: 'Pending', createdTime: '2017-10-23T13:54:00', completedTime: '2017-10-23T13:57:20',note:'Settle me now'},
  /*
    refcode: number;
    bankName: string;
    bankAccountNumber: string;
    bankAccountName: string;
    bankCode: string;
    bankBranch: string;
    currency: string;
    amount: number;
    status: string;
    createdTime: string;
    completedTime: string;
    note: string;

  PO Bill Number  Cart ID Payment Type  Currency  Order Amount  Transaction Amount  Transaction Charge  Status  Created Time  Completed Time  Action
PG-123  M-123 Bank Transfer CNY 10.00 9.91  0.9 Completed 2017-10-23 08:27:54 2017-10-23 08:54:01
   */
];