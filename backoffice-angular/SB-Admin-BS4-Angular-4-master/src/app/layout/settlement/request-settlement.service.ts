import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import 'rxjs/Rx';
import { settlement } from './settlement';
//import { SETTLEMENTS } from './mock-settlement';
import { Router } from '@angular/router';
import { BackofficeService } from '../../shared/services/backoffice.service';
import { response } from './response';

@Injectable()
export class RequestSettlementService {

 // private settlementsURL = 'http://localhost:8882/backoffice/api/transactions/settlements';  // URL to web api
  // private headers = new Headers({'Content-Type': 'application/json'});
  private methoddetails = new Array();
  private amt: number;
  url:string;
 constructor(private http: HttpClient,private router: Router,private bs: BackofficeService) { 
  this.generateURL();
 }

 private handleError(error: any): Promise<any> {
   console.error('An error occurred', error); // for demo purposes only
   return Promise.reject(error.message || error);
 }

 /*
  getMockSettlements(): Promise<settlement[]> {
    return Promise.resolve(SETTLEMENTS);
  }
  */
  
  generateURL(){
    //this.url = this.bs.getURL()+this.settlementURL;
    this.url = this.bs.getSettlementURL();
   }
  createSettlement(amount: number, bankname: string,bankbranch:string,accountnumber:string,accountname:string,note:string) {
    const id = localStorage.getItem("roleid");
    const username = localStorage.getItem("username");
    const headers = new HttpHeaders().set("x-username",username);
    let param = JSON.stringify({
      ["method"]: "BANK_TRANSFER",
      ["bankName"]: bankname,
      ["bankBranch"]: bankbranch,
      ["accountNumber"]: accountnumber,
      ["accountName"]: accountname,
    });
    this.amt = +amount;

       return this.http.post<any>(this.url, { 
        //HARDCODED FOR NOW: merchantid and currency and bank transfer method
     
        "merchantId":id,
        "currency": "CNY",
        "amount": this.amt.toFixed(2),
        "note": note,
        "methodDetails":JSON.parse(param),
        
        },{headers:headers}).subscribe(res => {
           
          this.router.navigate(['/settlement']);
         },
         err => {
           this.bs.handleError(err);
         });
    }

}
