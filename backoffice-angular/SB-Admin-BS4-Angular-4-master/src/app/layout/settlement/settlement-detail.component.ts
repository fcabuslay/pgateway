import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { SettlementDetail } from './settlement-detail';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service'

@Component({
    selector: 'app-settlement-detail',
    templateUrl: './settlement-detail.component.html',
    styleUrls: [ './settlement-detail.component.scss' ]
  })
  export class SettlementDetailComponent implements OnInit {
    settlement: SettlementDetail;
    SettlementForm: FormGroup;
    ProcessForm: FormGroup;
    post:any;
    modalRef: NgbModalRef;
    status: Observable<string>;

    constructor(
        private route: ActivatedRoute,
        private location: Location,
        private modalService: NgbModal,
        private authService: AuthService
    ) {}

    ngOnInit(): void {
        this.getSettlement();
        this.SettlementForm = new FormGroup({ 
        'id':  new FormControl(),
        'merchantid': new FormControl(),
        'amount': new FormControl(),
        'currency': new FormControl(),
        'method': new FormControl(),
        'status': new FormControl(),
        'lastnote': new FormControl(),
        'createdby': new FormControl(),
        'createddate': new FormControl(),
        'lastmodifiedby': new FormControl(),
        'lastmodifieddate': new FormControl(),
        'bankname': new FormControl(),
        'bankbranch': new FormControl(),
        'accountname': new FormControl(),
        'accountnumber': new FormControl(),
      });
      this.ProcessForm = new FormGroup({ 
        'status': new FormControl(),
        'note': new FormControl(),
      });
    }

    open(content) {
      this.modalRef = this.modalService.open(content);
    }

    process(content,status:string){
      this.process2(status).subscribe(status => this.status = status);
      this.ProcessForm.controls['status'].setValue(this.status);
      this.modalRef = this.modalService.open(content);
    }

    process2(status:string):Observable<any>{
      return  Observable.of(status);
    }
   
    getSettlement(): void {
      const id = +this.route.snapshot.paramMap.get('id');
      this.authService.getSettlement(id)
        .subscribe(settlement => this.settlement = settlement);
    }
    
    goBack(): void {
      this.location.back();
    }

    processSettlement(post){
      const id = +this.route.snapshot.paramMap.get('id');
      this.modalRef.close()
      this.authService.processSettlement2(id,post).subscribe(res => {
        this.authService.notify("Settlement successfully "+post.status,"success")
        this.getSettlement()
      },
        err => {
          this.authService.handleError(err);
        });
    }

      
    

   
  }