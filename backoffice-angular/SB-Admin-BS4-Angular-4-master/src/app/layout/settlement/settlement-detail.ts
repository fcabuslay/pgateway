//{"id":1,"merchantId":1,"amount":100.00000000,"currency":"CNY","method":"BANK_TRANSFER","status":"PENDING","lastNote":"Note","createdBy":"SYSTEM","createdDate":"2018-03-03 14:13:52","lastModifiedBy":"SYSTEM","lastModifiedDate":"2018-03-03 14:13:52","methodDetails":{"bankName":"ICBC","bankBranch":"Guang Zhou","accountName":"Account Holder Name","accountNumber":"1234567890","method":"BANK_TRANSFER"}}

export interface SettlementDetail {
    id:number;
    merchantid:number;
    amount: string;
    currency: string;
    method: string;
    status: string;
    lastnote: string;
    createdby: string;
    createddate: string;
    lastmodifiedby: string;
    lastmodifieddate: string;
    methoddetails: string;
}