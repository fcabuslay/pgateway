import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { SettlementRoutingModule } from './settlement-routing.module';
import { SettlementComponent } from './settlement.component';
import { PageHeaderModule } from './../../shared';
import { SettlementService } from './settlement.service';
import { DatePickerModule } from 'angular-io-datepicker';
import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';


@NgModule({
    imports: [
        CommonModule,
        SettlementRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        OverlayModule,
        DatePickerModule,
        NgxPaginationModule,
        Ng2SearchPipeModule,
        Ng2OrderModule
    ],
   declarations: [SettlementComponent],
   providers: [ SettlementService ],
   bootstrap: [ SettlementComponent ]
})
export class SettlementModule { }
