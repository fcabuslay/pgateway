import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap'
import { TranslateModule } from '@ngx-translate/core'
import { LayoutRoutingModule } from './layout-routing.module'
import { LayoutComponent } from './layout.component'
import { HeaderComponent, SidebarComponent } from '../shared'
import { CalendarModule } from 'primeng/primeng'
import { NgxPaginationModule } from 'ngx-pagination'
import { Ng2SearchPipeModule } from 'ng2-search-filter'
import { Ng2OrderModule } from 'ng2-order-pipe'
import { PayinsComponent } from '../layout/payins/payins.component'
import { PayinSearchComponent } from '../layout/payins/payin-search.component'
import { DashboardComponent } from '../layout/dashboard/dashboard.component'
import { PayoutsComponent } from './payouts/payouts.component'
import { PayoutSearchComponent } from './payouts/payout-search.component'
import { PayoutDetailComponent } from './payouts/payout-detail.component'
import { SettlementComponent } from './settlement/settlement.component'
import { SettlementDetailComponent } from './settlement/settlement-detail.component'
import { RequestSettlementComponent } from './settlement/request-settlement.component'
import { FieldErrorDisplayComponent } from './common/field-error-display.component'
import { ProfileComponent } from './profile/profile.component'
import { UserComponent } from './Users/user.component'
import { UserDetailComponent } from './Users/user-detail.component'
import { MerchantComponent } from './merchants/merchant.component'
import { MerchantDetailComponent } from './merchants/merchant-detail.component'
import { MerchantRateComponent } from './merchants/merchant-rate.component'
import { NotifyModule } from '../notify/notify.module'
import { SummaryComponent } from './summary/summary.component'
import { MidManagementComponent } from './mid-management/mid-management.component'
import { MidDetailComponent } from './mid-management/mid-detail.component'
import { RoutingComponent } from './routing/routing.component'
import { RoutingDetailComponent } from './routing/routing-detail.component'
import { MidFieldsComponent } from './mid-management/mid-fields.component'

@NgModule({

    declarations: [
        LayoutComponent,
        HeaderComponent,
        SidebarComponent,
        DashboardComponent,
        PayinsComponent,
        PayinSearchComponent,
        PayoutsComponent,
        PayoutSearchComponent,
        PayoutDetailComponent,
        SettlementComponent,
        SettlementDetailComponent,
        RequestSettlementComponent,
        FieldErrorDisplayComponent,
        ProfileComponent,
        UserComponent,
        UserDetailComponent,
        MerchantComponent,
        MerchantDetailComponent,
        MerchantRateComponent,
        SummaryComponent,
        MidManagementComponent,
        RoutingComponent,
        RoutingDetailComponent,
        MidDetailComponent,
        MidFieldsComponent
    ],
    imports: [
        CommonModule,
        NgbDropdownModule.forRoot(),
        LayoutRoutingModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        CalendarModule,
        NgxPaginationModule,
        Ng2SearchPipeModule,
        Ng2OrderModule,
        NotifyModule
        
    ],
    bootstrap: [LayoutComponent]

    
})
export class LayoutModule { }
