/*
status: Observable<string>;

passing observable to html , in this case it is status
    process(content,status:string){
      this.process2(status).subscribe(status => this.status = status);
      this.ProcessForm.controls['status'].setValue(this.status);
      this.modalRef = this.modalService.open(content);
    }
    process2(status:string):Observable<any>{
      return  Observable.of(status);
    }

get id from merchantcode, assign to merchantid
        getMerchantResponse(){ 
        return this.http.get<MerchantCode>
          (this.merchantsURL).subscribe(
            data => { this.merchantid = data.id; },
            (err) => {
              this.bs.handleError(err);
            }
          );
      }

get multiple stuff from subscribe in httpclient
private token: string;
private data: string;
    register(name:string, email:string,password:string){
        return this.http.post<any>(`${CONFIG.API_URL}/register`,{
            "name": name,
            "email":email,
            "password":password,
        }).subscribe(
            res => { 
                this.token = res.token,
                this.data = res.user.data,
                localStorage.setItem("token",this.token),
                localStorage.setItem("data",this.data),
                console.log(localStorage.getItem("token")) },
            err => {
              console.log("Error occured");
            }
          );
    }    
    
    
more ways of parsing stuff in httpclient
    register(name:string, email:string,password:string){
        return this.http.post<any>(`${CONFIG.API_URL}/register`,{
            "name": name,
            "email":email,
            "password":password,
        }).subscribe(
            res => { 
                let token = res.token
                let user = res.user.data
                let userData = new UserData(token,user)
                this.logUserIn(userData);
             },
            err => {
              console.log("Error occured");
            }
          );
    }

    logUserIn(userData: UserData):void{
        localStorage.setItem("token",userData.token)
        localStorage.setItem("user",JSON.stringify(userData.user))
        this.router.navigate(['/dashboard'])
    }
}

  //This is how you assign httpclient to get variables fyi
  getMerchantResponse(){ 
    return this.http.get<MerchantCode[]>
      (this.merchantsURL).subscribe(
        data => this.merchantcode = data,
        (err) => {
          this.bs.handleError(err);
        }
      );
  }
*/