import { Component, OnInit } from '@angular/core';
import { Statement } from './statement';
import { routerTransition } from '../../router.animations';
import { StatementService } from './statement.service';


@Component({
    selector: 'app-statement',
    templateUrl: './statement.component.html',
    styleUrls: ['./statement.component.scss'],
    providers: [StatementService],
    animations: [routerTransition()]
})
export class StatementComponent implements OnInit {
  statement: Statement[];

  constructor(private statements:StatementService) { }

  getStatement(): void {
    this.statements.getStatement().then(statement => this.statement = statement);
  }

  getMockStatement(): void {
    this.statements.getStatements().then(statement => this.statement = statement);
  }

  ngOnInit(): void {
    this.getMockStatement();
  }
}
