export interface Statement {
    id: number;
    type: string;     //Payin, Payout, Settlement
    typeid: string;   //Cart / Payout / Refno
    amount: number;
    paytype: string;  //Bank Transfer , wechat , quickpay, etc
    balance: number; //running balance
    startdate: string;
    enddate: string;
  }
  