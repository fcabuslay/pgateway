import { Statement } from './statement';

export const statement: Statement[] = [
  { id: 11, type: 'Payin', typeid: 'PG-123', paytype: 'Bank Transfer', amount:100, balance: 100, startdate: '2017-10-23T13:54:00', enddate: '017-10-23T13:54:00'},
  { id: 12, type: 'Payout', typeid: 'PO-456', paytype: '', amount:50, balance: 50, startdate: '2017-10-23T13:54:00', enddate: '017-10-23T13:54:00'},
  /*
    id: number;
    type: string;     //Payin, Payout, Settlement
    typeid: string;   //Cart / Payout / Refno
    paytype: string;  //Bank Transfer , wechat , quickpay, etc
    balance: number; //running balance
    startdate: string;
    enddate: string;
   */
];