import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { StatementRoutingModule } from './statement-routing.module';
import { StatementComponent } from './statement.component';
import { PageHeaderModule } from './../../shared';
import { StatementService } from './statement.service';
import { DatePickerModule } from 'angular-io-datepicker';
import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';



@NgModule({
    imports: [
        CommonModule,
        StatementRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        OverlayModule,
        DatePickerModule,
    ],
   declarations: [StatementComponent],
   providers: [ StatementService ],
   bootstrap: [ StatementComponent ]
})
export class StatementModule { }
