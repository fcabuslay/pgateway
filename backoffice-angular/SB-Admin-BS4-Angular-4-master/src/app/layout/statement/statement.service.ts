import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import { Statement } from './statement';
import { statement } from './mock-statement';

@Injectable()
export class StatementService {

  private statementURL = 'https://05b1cc36-cf7a-4400-9847-152507bdaa90.mock.pstmn.io/payins';  // URL to web api
  // private headers = new Headers({'Content-Type': 'application/json'});

 constructor(private http: Http) { }

   getStatement(): Promise<Statement[]> {
   return this.http.get(this.statementURL)
              .toPromise()
              .then(res => res.json() as Statement[])
              .catch(this.handleError);
 }

 private handleError(error: any): Promise<any> {
   console.error('An error occurred', error); // for demo purposes only
   return Promise.reject(error.message || error);
 }

  getStatements(): Promise<Statement[]> {
    return Promise.resolve(statement);
  }

}
