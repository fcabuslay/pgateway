/*
| id | Payin Id | 12345 |
| createdDateFrom | Created Date From (Inclusive) | 2017-11-15 14:25:59 |
| createdDateTo | Created Date To (Inclusive) | 2017-12-19 11:30:00 |
| currencies | Currencies (comma-separated for multiple)| CNY,USD |
| merchantIds | Merchant Ids (comma-separated for multiple)| 100,200 |
| merchantTransIds | Merchant Transaction Ids (comma-separated for multiple)| 998877,abc-def-ghi |
| methods | Payment Methods (comma-separated for multiple)| BANK_TRANSFER,WECHAT |
| statuses | Payment Status (comma-separated for multiple)| PENDING,SENT |
*/

export interface Search
{
id: string;
createdDateFrom: Date;
createdDateTo: Date;
currencies: string[];
merchantIds: string[];
merchantTransIds: string[];
methods: string[];
statuses: string;
}