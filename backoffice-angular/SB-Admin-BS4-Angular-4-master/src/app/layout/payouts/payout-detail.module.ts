import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { PayoutDetailRoutingModule } from './payout-detail-routing.module';
import { PayoutDetailComponent } from './payout-detail.component';
import { PageHeaderModule } from './../../shared';
import { DatePickerModule } from 'angular-io-datepicker';
import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
    imports: [
        CommonModule,
        PayoutDetailRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        OverlayModule,
        DatePickerModule,
        NgbModule
    ],
   declarations: [PayoutDetailComponent],
   providers: [ ],
   bootstrap: [ PayoutDetailComponent ]
})
export class PayoutDetailModule { }
