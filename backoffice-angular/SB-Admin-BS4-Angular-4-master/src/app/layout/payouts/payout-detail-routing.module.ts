import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PayoutDetailComponent } from './payout-detail.component';


const routes: Routes = [
    { path: '', component: PayoutDetailComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PayoutDetailRoutingModule { }
