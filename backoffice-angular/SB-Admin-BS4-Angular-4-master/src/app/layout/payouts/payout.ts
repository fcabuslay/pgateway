export interface Payout {
  id: number;
  apiVersion: string;
  merchantId: string;
  merchantTransId: string;
  amount: string;
  currency: number;
  payoutCode: number;
  method: number;
  status: string;
  payoutDetails: string;
  methodDetails: string;
}

/*
{
    "pageNum": 1,
    "itemCount": 2,
    "totalItems": 2,
    "totalPages": 1,
    "items": [
        {
            "id": 2,
            "apiVersion": "1.0",
            "merchantId": 2,
            "merchantTransId": "1529756874620",
            "amount": 10,
            "currency": "CNY",
            "payoutCode": "CNICBC",
            "method": "BANK_TRANSFER",
            "status": "PENDING",
            "payoutDetails": {
                "callbackUrl": "http://google.com"
            },
            "methodDetails": {
                "bank": "CNICBC",
                "customerName": "",
                "bankAccountName": "",
                "bankAccountNumber": "",
                "bankBranch": "",
                "bankCity": "",
                "bankProvince": "",
                "method": "BANK_TRANSFER"
            }
        },
        {
            "id": 1,
            "apiVersion": "1.0",
            "merchantId": 2,
            "merchantTransId": "1529756784902",
            "amount": 10,
            "currency": "CNY",
            "payoutCode": "CNICBC",
            "method": "BANK_TRANSFER",
            "status": "PENDING",
            "payoutDetails": {
                "callbackUrl": "http://google.com"
            },
            "methodDetails": {
                "bank": "CNICBC",
                "customerName": "John Doe",
                "bankAccountName": "??·?",
                "bankAccountNumber": "12345678",
                "bankBranch": "??",
                "bankCity": "??",
                "bankProvince": "??",
                "method": "BANK_TRANSFER"
            }
        }
    ]
}
*/