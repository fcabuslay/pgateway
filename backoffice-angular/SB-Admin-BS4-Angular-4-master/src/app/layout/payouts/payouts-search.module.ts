import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { PayoutSearchRoutingModule } from './payout-search-routing.module';
import { PayoutSearchComponent } from './payout-search.component';
import { PageHeaderModule } from './../../shared';
//import { DatePickerModule } from 'angular-io-datepicker';
//import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CalendarModule} from 'primeng/primeng';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';

//import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import { DateTimePickerModule } from 'ng-pick-datetime';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import {A2Edatetimepicker} from 'ng2-eonasdan-datetimepicker';



@NgModule({
    imports: [
        CommonModule,
        //A2Edatetimepicker,
        PayoutSearchRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        CalendarModule,
        NgbModule,
        Ng2SearchPipeModule,
        Ng2OrderModule,
        NgxPaginationModule
       // OverlayModule,
        //DatePickerModule,
      // DateTimePickerModule,
   //     NgbModule,
    ],
   declarations: [PayoutSearchComponent],
   providers: [ ],
   bootstrap: [ PayoutSearchComponent ]
})
export class PayoutSearchModule { }