import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { PayoutsRoutingModule } from './payouts-routing.module';
import { PayoutsComponent } from './payouts.component';
import { PageHeaderModule } from './../../shared';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule } from 'primeng/primeng';



@NgModule({
    imports: [
        CommonModule,
        //A2Edatetimepicker,
        PayoutsRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        CalendarModule,
        NgbModule,
        Ng2SearchPipeModule,
        Ng2OrderModule,
        NgxPaginationModule
       // OverlayModule,
        //DatePickerModule,
      // DateTimePickerModule,
   //     NgbModule,
    ],
   declarations: [PayoutsComponent],
   providers: [ ],
   bootstrap: [ PayoutsComponent ]
})
export class PayoutsModule { }
