import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Payout } from './payout';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { NgbModal,  NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service'


@Component({
    selector: 'app-payout-detail',
    templateUrl: './payout-detail.component.html',
    styleUrls: [ './payout-detail.component.scss' ]
  })
  
  export class PayoutDetailComponent implements OnInit {
    payout: Payout;
    PayoutForm: FormGroup;
    ProcessForm: FormGroup;
    post:any;
    modalRef: NgbModalRef;
    status: Observable<string>;

    constructor(
        private route: ActivatedRoute,
        private location: Location,
        private modalService: NgbModal,
        private authService: AuthService
    ) {}

    ngOnInit(): void {
        //this.generateURL();
        this.getPayout();
        this.PayoutForm = new FormGroup({ 
        'transid':  new FormControl(),
        'merchant': new FormControl(),
        'orderid': new FormControl(),
        'callbackurl': new FormControl(),
        'orderamt': new FormControl(),
        'currency': new FormControl(),
        'bankcode': new FormControl(),
        'status': new FormControl(),
        'method': new FormControl(),
        'bank': new FormControl(),
        'customername': new FormControl(),
        'bankacctname': new FormControl(),
        'bankacctno': new FormControl(),
        'bankbranch': new FormControl(),
        'bankcity': new FormControl(),
        'bankprovince': new FormControl(),
      });
      this.ProcessForm = new FormGroup({ 
        'status': new FormControl(),
        'note': new FormControl(),
      });
    }
   
    process(content,status:string){
      this.process2(status).subscribe(status => this.status = status);
      this.ProcessForm.controls['status'].setValue(this.status);
      this.modalRef = this.modalService.open(content);
    }

    process2(status:string):Observable<any>{
      return  Observable.of(status);
    }

    processPayout(post){
      const id = +this.route.snapshot.paramMap.get('id');
      //this.authService.processPayout(id,post)
      this.authService.processPayout2(id,post).subscribe( res => { 
        this.getPayout()
        
        this.modalRef.close()
        this.authService.notify("Payout successfully "+post.status,"success")
        err => {
          this.authService.handleError(err);
        }});  
    }

    open(content) {
      this.modalRef = this.modalService.open(content);
    }
    getPayout(): void {
      const id = +this.route.snapshot.paramMap.get('id');
      this.authService.getPayout(id)
        .subscribe(data => this.payout = data);
    }
    
    goBack(): void {
      this.location.back();
    }
  }