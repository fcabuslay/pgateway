import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PayoutSearchComponent } from './payout-search.component';
const routes: Routes = [
    { path: '', component: PayoutSearchComponent },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PayoutSearchRoutingModule { }
