import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { PayinSummary } from '../payins/payin-summary'

@Component({
    selector: 'app-summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit {
    admin: boolean
    merchant: string
    payment_method: string
    currency: string
    pending: string
    completed: string
    cancelled: string
    total: string
    successrate: string
    payinsummary: PayinSummary[]
    payoutsummary: PayinSummary[]
    

    constructor(
        private authService: AuthService
    ) {}

    ngOnInit(): void {
       this.isAdmin()
       this.getPayinSummary()
       this.getPayoutSummary()
    }
    isAdmin(){
        this.admin =  this.authService.isAdmin();
    }

    getPayinSummary(): void {

        this.authService.getPayinSummary().subscribe(data => {
          this.payinsummary = data
        }),
          (err) => {
            this.authService.handleError(err)
          }
    }

    getPayoutSummary(): void {
        this.authService.getPayoutSummary().subscribe(data => {
          this.payoutsummary = data 
        }),
          (err) => {
            this.authService.handleError(err)
          }
    }

}