export interface Summary {
  id: number;
  ponum: string;
  cartid: string;
  paytype: string;
  currency: string;
  orderamt: number;
  transamt: number;
  transcharge: number;
  status: string;
  startdate: string;
  enddate: string;
}
