import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import { Summary } from './summary';
import { SUMMARIES } from './mock-summary';

@Injectable()
export class SummaryService {

  private summaryURL = 'https://05b1cc36-cf7a-4400-9847-152507bdaa90.mock.pstmn.io/payins';  // URL to web api
  // private headers = new Headers({'Content-Type': 'application/json'});

 constructor(private http: Http) { }

   getSummary(): Promise<Summary[]> {
   return this.http.get(this.summaryURL)
              .toPromise()
              .then(res => res.json() as Summary[])
              .catch(this.handleError);
 }

 private handleError(error: any): Promise<any> {
   console.error('An error occurred', error); // for demo purposes only
   return Promise.reject(error.message || error);
 }

  getSummaries(): Promise<Summary[]> {
    return Promise.resolve(SUMMARIES);
  }

}
