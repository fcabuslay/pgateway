import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { SummaryRoutingModule } from './summary-routing.module';
import { SummaryComponent } from './summary.component';
import { PageHeaderModule } from './../../shared';
import { SummaryService } from './summary.service';
import { DatePickerModule } from 'angular-io-datepicker';
import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';



@NgModule({
    imports: [
        CommonModule,
        SummaryRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        OverlayModule,
        DatePickerModule,
    ],
   declarations: [SummaryComponent],
   providers: [ SummaryService ],
   bootstrap: [ SummaryComponent ]
})
export class SummaryModule { }
