import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { PageHeaderModule } from './../../shared';
import { ProfileService } from './profile.service';
//import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JsessionIDInterceptor } from '../../shared/services/jsession.interceptor';


@NgModule({
    imports: [
        CommonModule,
        ProfileRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        Ng2SearchPipeModule,
        Ng2OrderModule,
        NgxPaginationModule
        //OverlayModule,
    ],
   declarations: [ProfileComponent],
   providers: [ ProfileService ,{
    provide: HTTP_INTERCEPTORS,
    useClass: JsessionIDInterceptor ,
    multi: true
  }],
   bootstrap: [ ProfileComponent ]
})
export class ProfileModule { }
