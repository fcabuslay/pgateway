import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { User } from './user';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { BackofficeService } from '../../shared/services/backoffice.service';

@Injectable()
export class ProfileService {

  //private userURL = 'http://localhost:8882/backoffice/api/users';  // URL to web api

  private merchants = new Array();
  private role:number;
  private url:string;
 
 constructor(
  private http: HttpClient,
  private router: Router,
  private location: Location,
  private bs: BackofficeService,

) {
  this.generateURL();
 }

generateURL(){
  this.url = this.bs.getUsersURL()+"/profile";
}

 addUser(username: string, password: string,email:string,firstname:string,lastname:string,merchantids:number,roleid:number) {


/*
  this.newuser.firstName=username;
  this.newuser.password=password;
  this.newuser.email=email;
  this.newuser.firstName=firstname;
  this.newuser.lastName=lastname;
  this.newuser.merchant=merchantids;
  this.newuser.roleId=roleid;


    return this.http.post<NewUser>(this.userURL, this.newuser).subscribe(res => {
         this.router.navigate(['/users']);
       },
       err => {
         this.handleError(err);
       });
      */
     if (merchantids!=null){
      this.merchants.push(merchantids);
    }
      this.role = +roleid;
   return this.http.post<any>(this.url, {
    "username": username,
    "password": password,
    "email":email,
    "firstName":firstname,
    "lastName":lastname,
    "merchantIds":this.merchants,
    "roleId":this.role
    }).subscribe(res => {
       this.refreshPage();
     },
     err => {
       this.handleError(err);
     });
}

  editUser(id:number,firstname:string,lastname:string,email:string,roleid:number) {
    const url = `${this.url}/${id}`;
        this.role = +roleid;
     return this.http.put<any>(url, {
      "firstName":firstname,
      "lastName":lastname,
      "email":email,
      "roleId":this.role
      }).subscribe(res => {
         this.router.navigate(['/users']);
       },
       err => {
         this.handleError(err);
       });
  }
  disableUser(id:number){
    const url = `${this.url}/${id}/disable`;
    return this.http.put<any>(url,{}).subscribe(res => {
      this.router.navigate(['/users']);
    },
    err => {
      this.handleError(err);
    });
  }
  refreshPage() {
location.reload()
}
  enableUser(id:number){
    const url = `${this.url}/${id}/enable`;
    return this.http.put<any>(url,{}).subscribe(res => {
      this.router.navigate(['/users']);
    },
    err => {
      this.handleError(err);
    });
  }



 
 private handleError(error: any): Promise<any> {
   console.error('An error occurred', error); // for demo purposes only
   return Promise.reject(error.message || error);
 }

  getUserObservable(id: number): Observable<User> {
    //const url = `${this.url}/${id}`;
    return this.http.get<User>(this.url);
  }
}
