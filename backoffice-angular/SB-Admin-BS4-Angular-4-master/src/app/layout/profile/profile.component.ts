import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { AuthService } from '../../services/auth.service'
import { Location } from '@angular/common';
@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;

  constructor(
    private authService: AuthService,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.getUserObservable();
  }
  
  getUserObservable(): void {
    this.authService.getProfileResponse().subscribe(user => this.user = user , err => {
      this.authService.handleError(err);
    });
    
  }

  goBack(): void {
    this.location.back();
  }
}
