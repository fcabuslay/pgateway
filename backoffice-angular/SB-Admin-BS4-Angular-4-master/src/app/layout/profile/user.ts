export interface User {
    id: number;
    username: string;     
    password: string;
    email: string;
    enabled: string;
    firstName: string; 
    lastName: string; 
    merchant: number[];
    roleId: number;
    roleName: string;
    
  }
  
  /*
  "username": "admin",
  "password": "12345678",
  "email": "user@integration.com",
  "firstName": "Paul",
  "lastName": "Gasol",
  "merchant": [ 200, 201 ],
  "roleId": 550
  */