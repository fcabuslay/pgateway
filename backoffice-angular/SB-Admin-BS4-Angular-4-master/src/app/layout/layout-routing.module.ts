import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { RoleGuard} from '../shared/guard/role-guard';
import { PayinsComponent} from './payins/payins.component'
import { PayinSearchComponent } from './payins/payin-search.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { PayoutsComponent } from './payouts/payouts.component'
import { PayoutSearchComponent } from './payouts/payout-search.component'
import { PayoutDetailComponent } from './payouts/payout-detail.component'
import { SettlementComponent } from './settlement/settlement.component'
import { SettlementDetailComponent } from './settlement/settlement-detail.component'
import { RequestSettlementComponent } from './settlement/request-settlement.component'
import { ProfileComponent } from './profile/profile.component'
import { UserComponent } from './Users/user.component'
import { UserDetailComponent } from './Users/user-detail.component'
import { MerchantComponent } from './merchants/merchant.component'
import { MerchantDetailComponent } from './merchants/merchant-detail.component'
import { MerchantRateComponent } from './merchants/merchant-rate.component'
import { SummaryComponent } from './summary/summary.component'
import { MidManagementComponent } from './mid-management/mid-management.component'
import { MidDetailComponent } from './mid-management/mid-detail.component'
import { RoutingComponent } from './routing/routing.component'
import { RoutingDetailComponent } from './routing/routing-detail.component'
import { MidFieldsComponent } from './mid-management/mid-fields.component'

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            //{ path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            
            { path: 'dashboard', component: DashboardComponent },
            { path: 'summary', component: SummaryComponent },
            //{ path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            //{ path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            //{ path: 'forms', loadChildren: './form/form.module#FormModule' },
            //{ path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            //{ path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            //{ path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            //{ path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            //{ path: 'payins', loadChildren: './payins/payins.module#PayinsModule' },
            { path: 'payins', component: PayinsComponent },
            { path: 'payins/search', component: PayinSearchComponent }, 
            //  { path: 'payins/search', loadChildren: './payins/payins-search.module#PayinSearchModule' },
            //{ path: 'payouts', loadChildren: './payouts/payouts.module#PayoutsModule' },
            { path: 'payouts', component: PayoutsComponent }, 
            //{ path: 'payouts/search', loadChildren: './payouts/payouts-search.module#PayoutSearchModule' },
            { path: 'payouts/search', component: PayoutSearchComponent },
            { path: 'payouts/:id',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },        
                component: PayoutDetailComponent },         
            //{ path: 'summary', loadChildren: './summary/summary.module#SummaryModule' },
            //{ path: 'settlement', loadChildren: './settlement/settlement.module#SettlementModule' },
            { path: 'settlement', component: SettlementComponent },
            { path: 'settlement/:id',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },
                //loadChildren: './settlement/settlement-detail.module#SettlementDetailModule' },             
                component: SettlementDetailComponent } ,
            //{ path: 'request-settlement', loadChildren: './settlement/request-settlement.module#RequestSettlementModule' },
            { path: 'request-settlement', component: RequestSettlementComponent },
            //{ path: 'view-mdr', loadChildren: './mdr/mdr.module#MDRModule' },
            //{ path: 'view-statement', loadChildren: './statement/statement.module#StatementModule'},
            //{ path: 'profile', loadChildren: './profile/profile.module#ProfileModule'},
            { path: 'profile', component: ProfileComponent},
            { path: 'users',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },
                //loadChildren: './Users/user.module#UserModule' },
                component: UserComponent },
            { path: 'user/:id',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },        
                //loadChildren: './Users/user-detail.module#UserDetailModule' },
                component: UserDetailComponent },
            { path: 'merchants',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },
                //loadChildren: './merchants/merchant.module#MerchantModule' },
                component: MerchantComponent },

            { path: 'merchants/:id',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },        
                //loadChildren: './merchants/merchant-detail.module#MerchantDetailModule' },
                component: MerchantDetailComponent },
            { path: 'merchants/:id/rates',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },        
                //loadChildren: './merchants/merchant-rate.module#MerchantRateModule' },                        
                component: MerchantRateComponent },
            { path: 'midmanagement',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },        
                //loadChildren: './merchants/merchant-rate.module#MerchantRateModule' },                        
                component: MidManagementComponent }, 
            { path: 'routing',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },                     
                component: RoutingComponent }, 
            { path: 'midmanagement/:id',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },                     
                //loadChildren: './merchants/merchant-rate.module#MerchantRateModule' },                        
                component:  MidDetailComponent},     
                
            { path: 'midmanagement/:id/details',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },                     
                //loadChildren: './merchants/merchant-rate.module#MerchantRateModule' },                        
                component:  MidFieldsComponent},                     

            { path: 'routing/:id',
                canActivate: [RoleGuard],
                data: { 
                expectedRole: 'Administrator'
                },        
                //loadChildren: './merchants/merchant-rate.module#MerchantRateModule' },                        
                component: RoutingDetailComponent },                             
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [RoleGuard]
})
export class LayoutRoutingModule { }
