import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    constructor(public router: Router) { }

    ngOnInit() {
        if (this.router.url === '/') {
            if (localStorage.length > 0) {
                // We have items
                this.router.navigate(['/dashboard']);
              } else {
                // No items
                this.router.navigate(['/login']);
              }
            
        }
    }

}
