import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MerchantDetailComponent } from './merchant-detail.component';


const routes: Routes = [
    { path: '', component: MerchantDetailComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MerchantDetailRoutingModule { }
