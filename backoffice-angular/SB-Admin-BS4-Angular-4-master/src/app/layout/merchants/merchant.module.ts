import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { MerchantRoutingModule } from './merchant-routing.module';
import { MerchantComponent } from './merchant.component';
import { PageHeaderModule } from './../../shared';
import { MerchantService } from './merchant.service';
//import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';


@NgModule({
    imports: [
        CommonModule,
        MerchantRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        //OverlayModule,
        Ng2SearchPipeModule,
        Ng2OrderModule,
        NgxPaginationModule
    ],
   declarations: [MerchantComponent],
   providers: [ MerchantService ],
   bootstrap: [ MerchantComponent ]
})
export class MerchantModule { }
