import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MerchantRateComponent } from './merchant-rate.component';


const routes: Routes = [
    { path: '', component: MerchantRateComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MerchantRateRoutingModule { }
