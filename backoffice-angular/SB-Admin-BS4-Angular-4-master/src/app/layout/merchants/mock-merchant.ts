import { Merchant } from './merchant';

export const MERCHANTS: Merchant[] = [
    { id: 11, code:'Merchant 1',fullName:'Merchant 1',currencies:["CNY"],createdBy:'francis',createdDate:'2017-12-17 00:20:35',lastModifiedBy:'michael',lastModifiedDate:'2017-12-17 00:20:35'}
]
/*
    id: number;
    code: string;
    fullName: string;
    currencies: string[];
    createdBy: string[];
    createdDate: string;
    lastModifiedBy: string;
    lastModifiedDate: string;
*/