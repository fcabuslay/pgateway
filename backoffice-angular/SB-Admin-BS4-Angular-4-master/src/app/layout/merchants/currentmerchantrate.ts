export class CurrentMerchantRate {
    id: number;
    transactiontype: string;
    paymentMethod: string;
    percentage: number;
    effectivedatedrom: string;
    createdby: string;
    createddate: string;
    lastmodifiedby: string;
    lastmodifieddate: string;
  }


/*
{
    "id": 13,
    "transactionType": "PAYOUT",
    "paymentMethod": "BANK_TRANSFER",
    "percentage": 2,
    "effectiveDateFrom": "2018-07-03 16:55:00",
    "createdBy": "SYSTEM",
    "createdDate": "2018-07-03 16:53:24",
    "lastModifiedBy": "SYSTEM",
    "lastModifiedDate": "2018-07-03 16:53:24"
}
*/