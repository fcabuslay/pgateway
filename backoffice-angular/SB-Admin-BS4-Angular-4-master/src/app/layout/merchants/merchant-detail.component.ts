import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Merchant } from './merchant';
import { CurrentMerchantRate } from './currentmerchantrate';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Balance } from '../../shared/components/header/balance';
import { BalanceResponse } from '../../shared/components/header/balance-response';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'app-merchant-detail',
    templateUrl: './merchant-detail.component.html',
    styleUrls: [ './merchant-detail.component.scss' ]
  })
  export class MerchantDetailComponent implements OnInit {
    merchant: Merchant;
    currentpayinODrate: CurrentMerchantRate;
    currentpayinBTrate: CurrentMerchantRate;
    currentpayoutrate: CurrentMerchantRate;
    currentsettlementrate: CurrentMerchantRate;
    MerchantForm: FormGroup;
    MerchantRateForm: FormGroup;
    MerchantBalanceForm: FormGroup;
    UpdateBalanceForm:FormGroup;
    post:any;
    username:string;
    password:string;
    email:string;
    firstname:string;
    lastname:string;
    merchantids:number;
    roleid:number;
    runningb: Balance;
    balanceresponse: BalanceResponse;
    modalRef: NgbModalRef;
    amount: string;
    currency: string;
    private url:string;


    constructor(
        private route: ActivatedRoute,
        private location: Location,
        private router: Router,
        private modalService: NgbModal,
        private authService: AuthService,
    ) {
      this.MerchantForm = new FormGroup({ 
        'transid':  new FormControl(),
        'fullname': new FormControl(),
        'orderid': new FormControl(),
        'orderamt': new FormControl(),
        'currencies': new FormControl(),
        'createdby': new FormControl(),
        'createddate': new FormControl(),
        'lastmodifiedby': new FormControl(),
        'lastmodifieddate': new FormControl()
      });
      this.MerchantRateForm = new FormGroup({ 
        'payinpercentage':  new FormControl(),
        'payoutpercentage':  new FormControl(),
        'settlementpercentage':  new FormControl(),
      });
      this.MerchantBalanceForm = new FormGroup({ 
        'balance': new FormControl(),
      });
      this.UpdateBalanceForm = new FormGroup({
        'amount' : new FormControl(),
        'currency' : new FormControl(),
      });

    }

    ngOnInit(): void {
        this.getCurrentRate('PAYIN','BANK_TRANSFER').subscribe(currentpayinBTrate => this.currentpayinBTrate = currentpayinBTrate);
        this.getCurrentRate('PAYIN','ONLINE_DEBIT').subscribe(currentpayinODrate => this.currentpayinODrate = currentpayinODrate);
        this.getCurrentRate('PAYOUT','BANK_TRANSFER').subscribe(currentpayoutrate => this.currentpayoutrate = currentpayoutrate);
        this.getCurrentRate('SETTLEMENT','BANK_TRANSFER').subscribe(currentsettlementrate => this.currentsettlementrate = currentsettlementrate);
        this.getBalanceObservable().subscribe(res =>  this.runningb = res.balances);
        this.getUser();
    }

    UpdateBalance(post) {
      const id = +this.route.snapshot.paramMap.get('id');
      //return this.authService.UpdateBalance(id,post)
      this.modalRef.close();
      this.authService.UpdateBalance(id,post).subscribe(res => {
        this.authService.notify("Balance successfully updated","success")
        this.getBalanceObservable().subscribe(res =>  this.runningb = res.balances);
      },
        err => {
          this.authService.handleError(err);
        });
    
    }

   
    getBalanceObservable():any{
      const id = +this.route.snapshot.paramMap.get('id');
      return this.authService.getRunningBalance(id)
    }

    open(content) {
      this.modalRef = this.modalService.open(content,{ size: 'lg' });
    }

    getUser(): void {
      const id = +this.route.snapshot.paramMap.get('id');
      this.getMerchantObservable(id)
        .subscribe(merchant => this.merchant = merchant);
    }
    
    goBack(): void {
      this.location.back();
    }


    getAllRates(): void {
      const id = +this.route.snapshot.paramMap.get('id');
      this.router.navigate(['/merchants/'+id+'/rates']);
    }


    getMerchantObservable(id: number): any {
      return this.authService.getMerchant(id)
    }


    getCurrentRate(transactiontype:string ,paymentMethod: string):any{
      const id = +this.route.snapshot.paramMap.get('id');
      return this.authService.getCurrentRate(id,transactiontype,paymentMethod)
    }

  }