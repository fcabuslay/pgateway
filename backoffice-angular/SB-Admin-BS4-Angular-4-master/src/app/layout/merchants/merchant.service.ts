import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Merchant } from './merchant';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { BackofficeService } from '../../shared/services/backoffice.service';

@Injectable()
export class MerchantService {

    private merchantURL = 'http://localhost:8882/backoffice/api/merchants';  // URL to web api
    private currencies = new Array();
    private merchantsURL:string;


constructor(
    private http: HttpClient,
    private router: Router,
    private location: Location,
    private bs: BackofficeService
) {
  this.generateURL();
 }

ngOnInit(): void {
  this.generateURL();
  //this.getMerchantsObservable();
}

generateURL(){
  this.merchantsURL = this.bs.getMerchantsURL();
}

 addMerchant(code: string, name: string,cny:string,thb:string) {
    if (cny){
      this.currencies.push("CNY");
    }
    if (thb){
        this.currencies.push("THB");
    }

   return this.http.post<any>(this.merchantsURL, { 
  
    "code": code,
    "fullName": name,
    "currencies":this.currencies,
    }).subscribe(res => {
       
       this.refreshPage();
     },
     err => {
       this.handleError(err);
     });
}

  editMerchant(id:number,code: string, name: string,cny:string,thb:string) {
    const url = `${this.merchantURL}/${id}`;
    if (cny!=null){
        this.currencies.push(cny);
      }
      if (thb!=null){
          this.currencies.push(thb);
      }
     return this.http.post<any>(url, {
      "code":code,
      "fullName":name,
      "currencies":this.currencies,
      }).subscribe(res => {
        this.refreshPage();
         //this.router.navigate(['/users']);
       },
       err => {
         this.handleError(err);
       });
  }
  disableUser(id:number){
    const url = `${this.merchantURL}/${id}/disable`;
    return this.http.put<any>(url,{}).subscribe(res => {
      this.router.navigate(['/users']);
    },
    err => {
      this.handleError(err);
    });
  }
  refreshPage() {
    location.reload()
    }
  enableMerchant(id:number){
    const url = `${this.merchantURL}/${id}/enable`;
    return this.http.put<any>(url,{}).subscribe(res => {
      this.router.navigate(['/users']);
    },
    err => {
      this.handleError(err);
    });
  }
  
 getAllMerchants(): Observable<Merchant[]>{
  const params = new HttpParams().set('page', "0").set('size', "10");
  return this.http.get<Merchant[]>(this.merchantURL,{ params}).catch((err) => {
          
      // Do messaging and error handling here
      return Observable.throw(err)
  })
}
 private handleError(error: any): Promise<any> {
   console.error('An error occurred', error); // for demo purposes only
   return Promise.reject(error.message || error);
 }

  
  getMerchantsObservable(): Observable<Merchant[]> {
    const params = new HttpParams().set('page', "0").set('size', "10");
    return this.http.get<Merchant[]>(this.merchantURL,{ params})
  }
/*
  getUserObservable(id: number): Observable<User> {
    return Observable.of(user.find(usr => usr.id === id));
  }*/
  getMerchantObservable(id: number): Observable<Merchant> {
    const url = `${this.merchantURL}/${id}`;
    return this.http.get<Merchant>(url);
  }
}
