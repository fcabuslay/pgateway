import { CurrentMerchantRate } from './currentmerchantrate';
export class response {
    pageNum: number;
    itemCount: number;
    totalItems: number;
    totalPages: number;
    items: CurrentMerchantRate[];
  }