import { Component, OnInit } from '@angular/core';
import { CurrentMerchantRate } from './currentmerchantrate';
import { Observable } from 'rxjs/Observable';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Merchant } from '../merchants/merchant';
import { MerchantCode } from '../merchants/merchantcode-response';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { response } from './merchantrate-response';
import { BackofficeService } from '../../shared/services/backoffice.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-merchant-rate',
  templateUrl: './merchant-rate.component.html',
  styleUrls: ['./merchant-rate.component.scss'],
})

export class MerchantRateComponent implements OnInit {
  RateForm: FormGroup;
  post: any;    //form group   
  closeResult: string;
  pay$: Observable<CurrentMerchantRate[]>;
  merchants: Merchant[];
  merchantcode: MerchantCode[];
  createdDateFrom: string;
  createdDateTo: string;
  status: string;
  currency: string;
  myform: FormGroup;
  method: string;
  selectedmerchant: string;
  id: string;
  cartid: string;
  isCollapsed: boolean = false;
  modalRef: NgbModalRef;
  public users$: Observable<MerchantCode[]>
  //search, paginate, etc below:
  key: string = 'createdBy'; //set default
  p: number = 1;
  size: number = 5;
  reverse: boolean = false;
  clicked: boolean = false;
  total: number;
  loading: boolean;
  result: any;
  response: Observable<response>;
  private url: string;
  private merchantsURL: string;
  merchantrate: CurrentMerchantRate[];
  pobtrate: CurrentMerchantRate[];
  settlementbtrate: CurrentMerchantRate[];
  admin: Boolean;
  transactiontype: string;
  paymentmethod: string;
  percentage:string;
  effectivedatefrom:string;

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  open(content) {
    this.modalRef = this.modalService.open(content,{ size: 'lg' });
    /*
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });*/
  }

  onSubmit(): void {
    console.log('you submitted value:', this.selectedmerchant);
  }

  addRate(post) {
    this.effectivedatefrom = this.transform(post.effectivedatefrom);
    this.transactiontype = post.transactiontype;
    this.paymentmethod = post.paymentmethod;
    this.percentage = post.percentage;
    const id = +this.route.snapshot.paramMap.get('id');
    const ratesurl = this.url+'/'+id+'/rates';
    this.modalRef.close();
     return this.http.put<any>(ratesurl, { 
      "transactionType": this.transactiontype,
      "paymentMethod": this.paymentmethod,
      "percentage":this.percentage,
      "effectiveDateFrom":this.effectivedatefrom,
      }).subscribe(res => {
         this.refreshPage();
       },
       err => {
         this.bs.handleError(err);
       });  
  }

  refreshPage() {
    location.reload()
    }
  transform(value: string) {
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(value, 'yyyy-MM-dd H:mm:ss');
    return value;
  }
  isAdmin(){
    this.admin =  this.bs.isAdmin();
  }

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private bs: BackofficeService,
    private route: ActivatedRoute,

  ) 
  {
    
    this.RateForm = fb.group({
      'transactiontype': '',
      'paymentmethod': '',
      'percentage': '',
      'effectivedatefrom': ''
    });
  }

  generateURL() {
    this.url = this.bs.getMerchantsURL();
    this.merchantsURL = this.bs.getMerchantCodesURL();
  }

  ngOnInit(): void {
    this.generateURL();
    //this.getPayoutsNew(1);
    this.getPayinBTRates(1);
   // this.getMerchantsNew(1);
    this.getPayoutBTRates(1);
    this.getSettlementBTRates(1);
    this.isAdmin();
  }

  getPayoutsNew(page: number): void {
    this.response = this.getResponse(page, 10);
    this.parseMerchantRate(page, this.response).subscribe(merchantrate => this.merchantrate = merchantrate);
  }

  getPayinBTRates(page: number): void {
    this.response = this.getRateResponse('PAYIN','BANK_TRANSFER',page, 10);
    this.parseMerchantRate(page, this.response).subscribe(merchantrate => this.merchantrate = merchantrate);

  }

  getPayoutBTRates(page: number): void {
    this.response = this.getRateResponse('PAYOUT','BANK_TRANSFER',page, 10);
    this.parseMerchantRate(page, this.response).subscribe(pobtrate => this.pobtrate = pobtrate);
  }

  getSettlementBTRates(page: number): void {
    this.response = this.getRateResponse('SETTLEMENT','BANK_TRANSFER',page, 10);
    this.parseMerchantRate(page, this.response).subscribe(settlementbtrate => this.settlementbtrate = settlementbtrate);
  }

  getMerchantsNew(page: number): void {
    this.getMerchantResponse();
   //  this.parseMerchants(page, this.response).subscribe(merchants => this.merchants = merchants);
  }



  getResponse(page: number, size: number): Observable<any> {
    const params = new HttpParams().set('page', page.toString()).set('size', size.toString());
    return new Observable(observer => {
      this.http.get(this.url, { params }).map(
        (data: response) => data)
      .subscribe(data => {
        this.total = data.totalItems;
        observer.next(data);
        observer.complete();
      },
        error => {
          this.bs.handleError(error);
        }
      );
    });
  }

  //?transactionType=PAYOUT&paymentMethod=BANK_TRANSFER&page=0&size=5  
  getRateResponse(transtype:string,method:string,page: number, size: number): Observable<any> {
    const id = +this.route.snapshot.paramMap.get('id');
    const ratesurl = this.url+'/'+id+'/rates';
    const params = new HttpParams().set('transactionType',transtype).set('paymentMethod', method).set('page', page.toString()).set('size', size.toString());
    return new Observable(observer => {
      this.http.get(ratesurl, { params }).map(
        (data: response) => data)
      .subscribe(data => {
        this.total = data.totalItems;
        observer.next(data);
        observer.complete();
      },
        error => {
          this.bs.handleError(error);
        }
      );
    });
  }

  //This is how you assign httpclient to get variables fyi
  getMerchantResponse(){ 
    return this.http.get<MerchantCode[]>
      (this.merchantsURL).subscribe(
        data => this.merchantcode = data,
        (err) => {
          this.bs.handleError(err);
        }
      );
  }

  parseMerchantRate(page: number, any): Observable<CurrentMerchantRate[]> {
    const perPage = 10;
    this.p = page;
    const start = 0;
    const end = start + perPage;
    return any.map(
      (res: any) => res.items.slice(start, end)
    )
  }

  parseMerchants(page: number, any): Observable<Merchant[]> {
    const perPage = 10;
    this.p = page;
    const start = 0;
    const end = start + perPage;
    return any.map(
      (res: any) => res.items.slice(start, end)
    )
  }

}
