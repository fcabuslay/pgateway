import { Component, OnInit } from '@angular/core';
import { CurrentMerchantRate } from './currentmerchantrate';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service'
import { Location } from '@angular/common';

@Component({
  selector: 'app-merchant-rate',
  templateUrl: './merchant-rate.component.html',
  styleUrls: ['./merchant-rate.component.scss'],
})

export class MerchantRateComponent implements OnInit {
  RateForm: FormGroup;
  post: any;    //form group   
  closeResult: string;
  createdDateFrom: string;
  createdDateTo: string;
  status: string;
  currency: string;
  myform: FormGroup;
  method: string;
  selectedmerchant: string;
  id: string;
  cartid: string;
  isCollapsed: boolean = false;
  modalRef: NgbModalRef;
  //search, paginate, etc below:
  key: string = 'id'; //set default
  p: number = 1;
  size: number = 5;
  reverse: boolean = true;
  clicked: boolean = false;
  total: number;
  loading: boolean;
  result: any;
  private url: string;
  private merchantsURL: string;
  merchantrate: CurrentMerchantRate[];
  payinODrate: CurrentMerchantRate[];
  pobtrate: CurrentMerchantRate[];
  settlementbtrate: CurrentMerchantRate[];
  admin: Boolean;
  transactiontype: string;
  paymentmethod: string;
  percentage:string;
  effectivedatefrom:string;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private location: Location,
  ) 
  {
    
    /*this.RateForm = fb.group({
      'transactiontype': '',
      'paymentmethod': '',
      'percentage': '',
      'effectivedatefrom': ''
    });*/

  }

  ngOnInit(): void {
    this.RateForm = new FormGroup({
      transactiontype:  new FormControl(),
      paymentmethod:  new FormControl(),
      percentage:  new FormControl(),
      effectivedatefrom:  new FormControl(new Date()),
    })
    this.getPayinBTRates(1);
    this.getPayoutBTRates(1);
    this.getSettlementBTRates(1);
    this.getPayinODRates(1);
  }
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  goBack(): void {
    this.location.back();
  }

  open(content) {
    
    this.modalRef = this.modalService.open(content,{ size: 'lg' });
    
  }

  addRate(post) {
    this.effectivedatefrom = this.transform(post.effectivedatefrom);
    this.transactiontype = post.transactiontype;
    this.paymentmethod = post.paymentmethod;
    this.percentage = post.percentage;
    const id = +this.route.snapshot.paramMap.get('id');
    this.modalRef.close();
    //this.authService.addRate(id,this.effectivedatefrom,this.transactiontype,this.paymentmethod,this.percentage);
    this.authService.addRate2(id,this.effectivedatefrom,this.transactiontype,this.paymentmethod,this.percentage).subscribe(res => {
      this.authService.notify("Rate successfully added","success")
      this.getPayinBTRates(1);
      this.getPayoutBTRates(1);
      this.getPayinODRates(1);
      this.getSettlementBTRates(1); 
      //this.authService.notify("Rate successfully added","success")   
      this.RateForm.controls['effectivedatefrom'].reset()
    },
      err => {
        this.authService.handleError(err);
      });
  }

  transform(value: string) {
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(value, 'yyyy-MM-dd H:mm:ss');
    return value;
  }

  getPayinBTRates(page: number): void {
    this.p = page
    const id = +this.route.snapshot.paramMap.get('id')
    this.authService.getAllRates(id,'PAYIN','BANK_TRANSFER', page).subscribe(data => {
      this.merchantrate = data.items,
      this.total = data.totalItems
    }),
      (err) => {
        this.authService.handleError(err)
      }
  }

  getPayinODRates(page: number): void {
    this.p = page
    const id = +this.route.snapshot.paramMap.get('id')
    this.authService.getAllRates(id,'PAYIN','ONLINE_DEBIT', page).subscribe(data => {
      this.payinODrate = data.items,
      this.total = data.totalItems
    }),
      (err) => {
        this.authService.handleError(err)
      }
  }

  getPayoutBTRates(page: number): void {
    this.p = page
    const id = +this.route.snapshot.paramMap.get('id')
    this.authService.getAllRates(id,'PAYOUT','BANK_TRANSFER', page).subscribe(data => {
      this.pobtrate = data.items,
      this.total = data.totalItems
    }),
      (err) => {
        this.authService.handleError(err)
      }
  }

  getSettlementBTRates(page: number): void {
    this.p = page
    const id = +this.route.snapshot.paramMap.get('id')
    this.authService.getAllRates(id,'SETTLEMENT','BANK_TRANSFER', page).subscribe(data => {
      this.settlementbtrate = data.items,
      this.total = data.totalItems
    }),
      (err) => {
        this.authService.handleError(err)
      }
  }

}
