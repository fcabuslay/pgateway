import { Merchant } from './merchant';
export class response {
    pageNum: number;
    itemCount: number;
    totalItems: number;
    totalPages: number;
    items: Merchant[];
  }