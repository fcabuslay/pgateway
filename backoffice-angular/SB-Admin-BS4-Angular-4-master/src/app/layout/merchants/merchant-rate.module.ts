import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { MerchantRateRoutingModule } from './merchant-rate-routing.module';
import { MerchantRateComponent } from './merchant-rate.component';
import { PageHeaderModule } from './../../shared';
import { DatePickerModule } from 'angular-io-datepicker';
import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { CalendarModule } from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        MerchantRateRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        OverlayModule,
        DatePickerModule,
        Ng2SearchPipeModule,
        Ng2OrderModule,
        NgxPaginationModule,
        CalendarModule
    ],
   declarations: [MerchantRateComponent],
   providers: [ ],
   bootstrap: [ MerchantRateComponent ]
})
export class MerchantRateModule { }
