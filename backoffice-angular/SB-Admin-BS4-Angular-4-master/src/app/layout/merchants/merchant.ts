export class Merchant {
    id: number;
    code: string;
    fullName: string;
    currencies: string[];
    createdBy: string;
    createdDate: string;
    lastModifiedBy: string;
    lastModifiedDate: string;
  }

  /*
  Create:
  "code": "MERCHANT123",
  "fullName": "Merchant 123",
  "currencies": ["CNY", "THB"]
  */

  /*
response:
    "id": 1,
    "code": "MERCHANT123",
    "fullName": "Merchant 123",
    "currencies": [
        "CNY",
        "THB"
    ],
    "createdBy": "SYSTEM",
    "createdDate": "2017-12-17 00:20:35",
    "lastModifiedBy": "SYSTEM",
    "lastModifiedDate": "2017-12-17 00:20:35"
  */