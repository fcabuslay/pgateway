export interface MerchantCode {
    id: number;
    code: string;
    name: string;
  }

  /*        "id": 29,
        "code": "merchant29",
        "name": "Merchant 29"
*/