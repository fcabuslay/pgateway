import { Component, OnInit } from '@angular/core';
import { Merchant } from './merchant';
import { Observable } from 'rxjs/Observable';
import { NgbModal,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup,FormBuilder } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { CONFIG } from '../../config/config'

@Component({
    selector: 'app-merchant',
    templateUrl: './merchant.component.html',
    styleUrls: ['./merchant.component.scss'],
    //animations: [routerTransition()]
})
export class MerchantComponent implements OnInit {
  merchant: Merchant[];
  AddMerchantForm: FormGroup;
  post:any;
  modalRef: NgbModalRef;
  code:string;
  name:string;
  cny:string;
  thb:string;

  key: string = 'id'; 
  p: number = 1;
  reverse: boolean = true;
  clicked: boolean = false;
  total:  number ;
  loading: boolean;
  result: any;


  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private authService: AuthService
  ) { 

    this.AddMerchantForm = fb.group({
      'code' : '',
      'name' : '',
      'cny' : '',
      'thb':'',
      'lastname':'',
      'merchantids':'',
      'roleid':'',
    });
  }

  ngOnInit(): void {
    this.getMerchantsNew(1);
  }

  sort(key){
    this.key = key; 
    this.reverse = !this.reverse;
  }


  open(content) {
    this.modalRef = this.modalService.open(content,{ size: 'lg' });
  }

  addPost(post) {
    this.code = post.code;
    this.name = post.name;
    this.cny = post.cny;
    this.thb = post.thb;
    this.modalRef.close();
    this.authService.addMerchant(this.code,this.name,this.cny ,this.thb).subscribe(res => {
      this.authService.notify("Merchant successfully created","success")
      this.getMerchantsNew(1);
    },
      err => {
        this.authService.handleError(err);
      });
  }

  getMerchantsNew(page: number): void {
    this.p = page
    this.authService.getResponse(`${CONFIG.MERCHANT_URL}`, page, 10).subscribe(data => {
      this.merchant = data.items,
      this.total = data.totalItems
    }),
      (err) => {
        this.authService.handleError(err)
      }
  }




}
