import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { MerchantDetailRoutingModule } from './merchant-detail-routing.module';
import { MerchantDetailComponent } from './merchant-detail.component';
import { PageHeaderModule } from './../../shared';
import { DatePickerModule } from 'angular-io-datepicker';
import { OverlayModule } from 'angular-io-overlay';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';



@NgModule({
    imports: [
        CommonModule,
        MerchantDetailRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        OverlayModule,
        DatePickerModule,
    ],
   declarations: [MerchantDetailComponent],
   providers: [ ],
   bootstrap: [ MerchantDetailComponent ]
})
export class MerchantDetailModule { }
