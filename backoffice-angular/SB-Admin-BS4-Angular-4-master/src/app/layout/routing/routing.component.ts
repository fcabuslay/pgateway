import { Component, OnInit } from '@angular/core';
import { RoutingRule } from './routingrule';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MerchantCode } from '../merchants/merchantcode-response';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service'
import { CONFIG } from '../../config/config'
import { MidCode } from '../mid-management/midcode'

@Component({
  selector: 'app-routing',
  templateUrl: './routing.component.html',
  styleUrls: ['./routing.component.scss'],
})

export class RoutingComponent implements OnInit {
  SearchForm: FormGroup;
  post: any;    //form group   
  closeResult: string;
  merchantcode: MerchantCode[];
  createdDateFrom: string;
  createdDateTo: string;
  transactiontype: string;
  currency: string;
  method: string;
  selectedmerchant: string;
  code: string;
  cartid: string;
  modalRef: NgbModalRef;
  //search, paginate, etc below:
  key: string = 'id'; //set default
  p: number = 1;
  reverse: boolean = true;
  size: number = 5;
  total: number;
  result: any;
  routingrules: RoutingRule[];
  midcodes: MidCode[];
  

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService

  ) {
    this.SearchForm = fb.group({
      'merchantCode': '',
      'transactionType': '',
      'method': '',
      'currency': '',
      'midConfigCode': '',
      'weight': '',
    });
  }

  ngOnInit(): void {
    this.getPayinMids(1)
    this.getMerchantCodes()
    this.getMidCodes()
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
  }

  addPost(post) {
    this.modalRef.close()
    this.authService.createRoutingRule(post).subscribe(res => {
      this.authService.notify("Routing Rule successfully updated","success")
      this.getPayinMids(1)
    },
      err => {
        this.authService.handleError(err);
      });
  
    /*
    this.authService.createRoutingRule2(post).subscribe( res => { 
      this.getPayinMids(1)
      this.getMerchantCodes()
      this.getMidCodes()
      this.authService.notify("Routing Rule successfully created","success")
      err => {
        this.authService.handleError(err);
      }});
      */
  //    this.modalRef.close();
  }



  getPayinMids(page: number): void {
    this.p = page
    this.authService.getResponse(`${CONFIG.ROUTING_RULE_URL}`, page, 10).subscribe(data => {
      this.routingrules = data.items,
      this.total = data.totalItems
    }),
      (err) => {
        this.authService.handleError(err)
      }
  }

  getMerchantCodes() {
    this.authService.getMerchantCode().subscribe(
      data => this.merchantcode = data,
      (err) => {
        this.authService.handleError(err);
      }
    );
  }

  getMidCodes() {
    this.authService.getMidCodes().subscribe( data => this.midcodes = data ,
    (err) => {
      this.authService.handleError(err);
    }
  );
  }

}
