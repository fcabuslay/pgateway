export interface RoutingRule {
    id: number;
    merchantCode: string;
    enabled: string;
    transactionType: string; 
    method: string;
    currency: string;
    midConfigCode: string;
    weight: number
  }