import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { RoutingRule } from './routingrule';
import { FormGroup, FormControl ,FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service';
import { MerchantCode } from '../merchants/merchantcode-response';
import { MidCode } from '../mid-management/midcode'
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-routing-detail',
    templateUrl: './routing-detail.component.html',
    styleUrls: [ './routing-detail.component.scss' ]
  })

  export class RoutingDetailComponent implements OnInit {
    routingrule: RoutingRule;
    RoutingForm: FormGroup;
    RoutingModal: FormGroup;
    control: FormControl;
    post:any;
    merchantcode: MerchantCode[];
    midcodes: MidCode[];
    modalRef: NgbModalRef;
    merchant: string;
    mid: string;
    handler: any;

    constructor(
        private route: ActivatedRoute,
        private location: Location,
        private router: Router,
        private modalService: NgbModal,
        private authService: AuthService,
        private fb: FormBuilder,
    ) {
      this.RoutingForm =fb.group({
        'id': '',
        'merchantCode': '',
        'transactionType': '',
        'method': '',
        'currency': '',
        'midConfigCode': '',
        'weight': '',
        'enabled': '',
      });

     
      this.RoutingModal = fb.group({
        'merchantcode': '',
        'midconfigcode': '',
        'weight': '',
      });

    }

    //open(content,merchant:string,mid:string) {
      open(content)  {
        //this.process(merchant).subscribe(status => this.merchant = status);
        //this.RoutingModal.setControl.u['merchantCode'].updateValueAndValidity("d")
        //this.RoutingModal.controls['midConfigCode'].setValue(mid);
        this.modalRef = this.modalService.open(content);
      }


    ngOnInit(): void {
        this.getRoutingRule()
        this.getMerchantCodes()
        this.getMidCodes()
    }

    getRoutingRule() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.authService.getRoutingRule(id).subscribe(data => this.routingrule = data )
    }

    updateRoutingRule(post){
      const id = +this.route.snapshot.paramMap.get('id');
      this.modalRef.close()
      this.authService.updateRoutingRule(id,post).subscribe(res => {
        this.authService.notify("Routing Rule successfully updated","success")
        this.getRoutingRule()
        //  return res;
      },
        err => {
          this.authService.handleError(err);
        });
      
      //this.authService.notify("Routing Rule successfully updated","success")
      /*
      return this.authService.updateRoutingRule2(id,post).subscribe( res => { 
        this.getRoutingRule()
        this.authService.notify("Routing Rule successfully updated","success")
        err => {
          this.authService.handleError(err);
        }});
        */
    }

    enableRoutingRule(){
      const id = +this.route.snapshot.paramMap.get('id');
      //return this.authService.enableRoutingRule(id)
      return this.authService.enableRoutingRule2(id).subscribe( res => { 
        this.getRoutingRule()
        this.authService.notify("Routing Rule successfully enabled","success")
        err => {
          this.authService.handleError(err);
        }});
    }

    disableRoutingRule(){
      const id = +this.route.snapshot.paramMap.get('id');
      //return this.authService.disableRoutingRule(id)
      return this.authService.disableRoutingRule2(id).subscribe( res => { 
        this.getRoutingRule()
        this.authService.notify("Routing Rule successfully disabled","success")
        err => {
          this.authService.handleError(err);
        }});
    }

    deleteRoutingRule(){
      const id = +this.route.snapshot.paramMap.get('id');
      return this.authService.deleteRoutingRule(id)
    }
    UpdateBalance(post) {
      const id = +this.route.snapshot.paramMap.get('id');
      return this.authService.UpdateBalance(id,post)
    }
    
    goBack(): void {
      this.location.back();
    }

    getMerchantCodes() {
        this.authService.getMerchantCode().subscribe(
          data => this.merchantcode = data,
          (err) => {
            this.authService.handleError(err);
          }
        );
      }

      compareMerchantCode(c1: MerchantCode, c2: RoutingRule): boolean {
        return c1 && c2 ? c1.name === c2.merchantCode : c1.name === c2.merchantCode;
    }
    
      getMidCodes() {
        this.authService.getMidCodes().subscribe( data => this.midcodes = data ,
        (err) => {
          this.authService.handleError(err);
        }
      );
      }

  }