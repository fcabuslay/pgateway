import { Injectable,OnInit } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';

@Injectable()
export class GatewayService {

    private userURL = 'http://localhost:8881/login'; 


    constructor(
    private http: HttpClient,
    private router: Router) {
    }

    Login(username: string, password: string) {
       const headers = new HttpHeaders().set("Content-Type","application/x-www-form-urlencoded");
       const params = new HttpParams()
       .set('username',username)
       .set('password',password);
/*
    return this.http.post<Payin[]>(this.payinsURL, params,{
            headers: headers
          }*/
        return this.http.post<any>(this.userURL, params,{headers: headers}).subscribe(res => {
            this.router.navigate(['/dashboard']);
          },
          err => {
            this.handleError(err);
          });
    }

    private handleError(error: any): Observable<any> {
        console.log('An error occurred', error); // for demo purposes only
        return Observable.throw(error);
    }

}