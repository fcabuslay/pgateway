import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {  
    FormsModule,  
    ReactiveFormsModule  
  } from '@angular/forms';
import { GatewayRoutingModule } from './gateway-routing.module';
import { GatewayComponent } from './gateway.component';
import { GatewayService } from './gateway.service';

@NgModule({
    imports: [
        CommonModule,
        GatewayRoutingModule,
        FormsModule,        
        ReactiveFormsModule 
    ],
    declarations: [GatewayComponent],
    providers: [ GatewayService ],
    bootstrap: [ GatewayComponent ]
})
export class GatewayModule {
}
