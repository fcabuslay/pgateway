import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormControl, FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GatewayService} from './gateway.service';

@Component({
    selector: 'app-gateway',
    templateUrl: './gateway.component.html',
    styleUrls: ['./gateway.component.scss'],
    animations: [routerTransition()],
    providers: [GatewayService],
})
export class GatewayComponent implements OnInit {
    LoginForm: FormGroup;
    username: string;
    password: string;
    post:any; 
    constructor(private router: Router,
        private fb: FormBuilder,
        private ls: GatewayService,
    ) {

            this.LoginForm = fb.group({
                'username' : '',
                'password' : '',
              });
    }

    ngOnInit() : void {
    }

    Login(post) {
        this.username = post.username;
        this.password = post.password;
        this.ls.Login(this.username,this.password);
        localStorage.setItem('isLoggedin', 'true');
    }

}
