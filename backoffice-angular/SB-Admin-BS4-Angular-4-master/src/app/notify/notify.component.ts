import { Component } from '@angular/core';
import { Message } from '../classes/Message'
import { NotifyService } from '../services/notify.service'

@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.scss']
})
export class NotifyComponent {
  message: Message;

  constructor(
    private notifyService: NotifyService
  ) { 
    this.notifyService.newMessageReceived.subscribe((message)=>this.newMessageReceived(message))
  }

  newMessageReceived(message:Message){
    this.message = message

    setTimeout(() => {
      this.message = new Message('','')
    },5000)
  }
}