import { Injectable } from '@angular/core'
import { CONFIG } from '../config/config'
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { NotifyService } from './notify.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map' 

@Injectable()
export class AuthService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private notifyService: NotifyService,
  ) { }

  isAdmin() {
    const roleid = localStorage.getItem("roleid");
    if (roleid == '1') {
      return true;
    }
    else {
      return false;
    }
  }

  notify( message: string, type: string ){
    this.notifyService.notify(message, type)
  }

  login(username: string, password: string) {
    const headers = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded");
    const params = new HttpParams()
      .set('username', username)
      .set('password', password);
    return this.http.post<any>(`${CONFIG.API_URL}/login`, params, { headers: headers }).subscribe(
      data => {
        localStorage.setItem("role", data.role.name);
        localStorage.setItem("roleid", data.role.id);
        localStorage.setItem("id", data.id);
        localStorage.setItem("username", data.username);
        //one below is for getting balance of merchant
        localStorage.setItem("merchant", JSON.stringify(data.merchants));
        localStorage.setItem('isLoggedin', 'true');
        this.router.navigate(['/dashboard']);
      },
      (err: HttpErrorResponse) => {
        if (err.status == 401) {
          this.notifyService.notify('Invalid Username and/or Password', 'error')
        }
        else if (err.status == 500 || err.status == 400 || err.status == 504) {
          this.notifyService.notify('Something went wrong. Please try again later', 'error')
        }
        else {
          this.notifyService.notify('Something went wrong. Please try again later', 'error')
        }
      }
    );

  }

  getResponse(url: string, page: number, size: number): any {
    const params = new HttpParams().set('page', page.toString()).set('size', size.toString());
    return this.http.get(`${CONFIG.API_URL}${url}`, { params })
  }

  getResponse2(url: string, page: number, size: number): any {
    const params = new HttpParams().set('page', page.toString()).set('size', size.toString());
     this.http.get(`${CONFIG.API_URL}${url}`, { params }).subscribe(res => {
       return Observable.of(res)
      //return this.http.put<any>(url,{ "status": this.status}).subscribe(res => {
    },
      err => {
        return Observable.of(err)
      });
  }

  getMerchantCode(): any {
    return this.http.get(`${CONFIG.API_URL}${CONFIG.MERCHANTCODE_URL}`)
  }

  getMidCodes(): any {
    return this.http.get(`${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}`+ '/codes')
  }

  getPSPConfigs(): any {
    return this.http.get(`${CONFIG.API_URL}${CONFIG.PSP_CONFIG_URL}`)
  }

  handleError(error: any) {
    //try the one above for now
    //this.router.navigate(['/login'], { queryParams: { err: 'yes' } });
    if (error.status == 500) {    
      this.router.navigate(['/login']);
    }

    if (error.status == 401) {    
      this.router.navigate(['/login']);
    }

    if (error.status == 400) {    
      this.notifyService.notify(error.error.error, 'error')
    }

    else {                
      this.router.navigate(['/login']);
  }
  }



  getProfileResponse(): any {
    return this.http.get(`${CONFIG.API_URL}${CONFIG.PROFILE_URL}`)
  }

  getBalance(): any {
    if (localStorage.getItem("merchant") != null) {
      const obj = JSON.parse(localStorage.getItem("merchant"));
      const id = obj[0].id;
      const url = `${CONFIG.API_URL}${CONFIG.MERCHANT_URL}/${id}` + '/wallets';
      return this.http.get(url);
    }
  }

  getPayinsFromSearch(page: number, result: any): any {
    const url = `${CONFIG.API_URL}${CONFIG.PAYIN_URL}` + '/search?page=' + page + '&size=10';
    const headers = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded");
    const params = new HttpParams()
      .set('createdDateFrom', result['createdDateFrom'])
      .set('createdDateTo', result['createdDateTo'])
      .set('id', result['id'])
      .set('merchantTransIds', result['cartid'])
      .set('currencies', result['currency'])
      .set('merchantCodes', result['selectedmerchant'])
      .set('methods', result['method'])
      .set('statuses', result['status']);
    return this.http.post(url, params, { headers })
  }

  getPayinSummary():any {
    const url = `${CONFIG.API_URL}${CONFIG.PAYIN_URL}` + '/summary'
    return this.http.get(url)
  }

  getPayoutSummary():any {
    const url = `${CONFIG.API_URL}${CONFIG.PAYOUT_URL}` + '/summary'
    return this.http.get(url)
  }

  getPayoutsFromSearch(page: number, result: any): any {
    const url = `${CONFIG.API_URL}${CONFIG.PAYOUT_URL}` + '/search?page=' + page + '&size=10';
    const headers = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded");
    const params = new HttpParams()
      .set('createdDateFrom', result['createdDateFrom'])
      .set('createdDateTo', result['createdDateTo'])
      .set('id', result['id'])
      .set('merchantTransIds', result['cartid'])
      .set('currencies', result['currency'])
      .set('merchantCodes', result['selectedmerchant'])
      .set('methods', result['method'])
      .set('statuses', result['status']);
    return this.http.post(url, params, { headers })
  }

  getPayout(id: number): any {
    const url = `${CONFIG.API_URL}${CONFIG.PAYOUT_URL}/${id}`;
    return this.http.get(url)
  }

  processPayout(id: number, post) {
    const url = `${CONFIG.API_URL}${CONFIG.PAYOUT_URL}/${id}`;
    let status = post.status;
    let note = post.note;
    return this.http.put<any>(url, { "status": status, "note": note }).subscribe(res => {
      //return this.http.put<any>(url,{ "status": this.status}).subscribe(res => {
      location.reload()
    },
      err => {
        this.handleError(err);
      });
  }

  processPayout2(id: number, post) {
    const url = `${CONFIG.API_URL}${CONFIG.PAYOUT_URL}/${id}`;
    let status = post.status;
    let note = post.note;
    return this.http.put<any>(url, { "status": status, "note": note }).map(res => {
      //return this.http.put<any>(url,{ "status": this.status}).subscribe(res => {
      return res;
    },
      err => {
        this.handleError(err);
      });
  }

  processMid(post){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}`;
    let code = post.code;
    let transactiontype = post.transactiontype;
    let method = post.method;
    let currency = post.currency;
    let minamount = post.minamount;
    let maxamount = post.maxamount;
    let pspconfig = post.pspconfig;
    return this.http.put<any>(url, { "code": code, "pspConfigCode":pspconfig, "transactionType": transactiontype, "method" :method, "limits": [ { "currency" : currency, "minAmount": minamount,"maxAmount":maxamount  }] }).map(res => {
      //return this.http.put<any>(url,{ "status": this.status}).subscribe(res => {
      return res;
    },
      err => {
        this.handleError(err);
      });
  }

  processMid2(post){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}`;
    let code = post.code;
    let transactiontype = post.transactiontype;
    let method = post.method;
    let currency = post.currency;
    let minamount = post.minamount;
    let maxamount = post.maxamount;
    let pspconfig = post.pspconfig;
    return this.http.put<any>(url, { "code": code, "pspConfigCode":pspconfig, "transactionType": transactiontype, "method" :method, "limits": [ { "currency" : currency, "minAmount": minamount,"maxAmount":maxamount  }] })
  }

  getSettlement(id: number): any {
    const url = `${CONFIG.API_URL}${CONFIG.SETTLEMENT_URL}/${id}`;
    return this.http.get(url)
  }

  processSettlement(id: number, post) {
    const url = `${CONFIG.API_URL}${CONFIG.SETTLEMENT_URL}/${id}`;
    let status = post.status;
    let note = post.note;
    //return this.http.put<any>(url, { "status": status, "note": note }).subscribe(res => {
      return this.http.put<any>(url,{ "status": status}).subscribe(res => {
      location.reload()
    },
      err => {
        this.handleError(err);
      });
  }

  processSettlement2(id: number, post) {
    const url = `${CONFIG.API_URL}${CONFIG.SETTLEMENT_URL}/${id}`;
    let status = post.status;
    let note = post.note;
    return this.http.put<any>(url, { "status": status, "note": note }).map(res => {
      //this.notify("Settlement successfully processed","success")
      return res;
    },
      err => {
        this.handleError(err);
      });
    //  return this.http.put<any>(url,{ "status": status})
  }
  createRoutingRule(post){
    const url = `${CONFIG.API_URL}${CONFIG.ROUTING_RULE_URL}`;
    let merchantCode = post.merchantCode;
    let transactionType = post.transactionType;
    let method = post.method;
    let currency = post.currency;
    let midConfigCode = post.midConfigCode;
    let weight = post.weight;
    return this.http.post<any>(url, { "merchantCode": merchantCode, "transactionType": transactionType,"method":method,"currency": currency,"midConfigCode":midConfigCode,"weight":weight }).map(res => {
      //return this.http.put<any>(url,{ "status": this.status}).subscribe(res => {
      //location.reload()
      return res;
    },
      err => {
        this.handleError(err);
      });
  }

  createRoutingRule2(post){
    const url = `${CONFIG.API_URL}${CONFIG.ROUTING_RULE_URL}`;
    let merchantCode = post.merchantCode;
    let transactionType = post.transactionType;
    let method = post.method;
    let currency = post.currency;
    let midConfigCode = post.midConfigCode;
    let weight = post.weight;
    return this.http.post<any>(url, { "merchantCode": merchantCode, "transactionType": transactionType,"method":method,"currency": currency,"midConfigCode":midConfigCode,"weight":weight })
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      //console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  createSettlement(amount: number, bankname: string, bankbranch: string, accountnumber: string, accountname: string, note: string) {
    const id = localStorage.getItem("roleid")
    const username = localStorage.getItem("username")
    const headers = new HttpHeaders().set("x-username", username)
    let param = JSON.stringify({
      ["method"]: "BANK_TRANSFER",
      ["bankName"]: bankname,
      ["bankBranch"]: bankbranch,
      ["accountNumber"]: accountnumber,
      ["accountName"]: accountname,
    });
    let amt = +amount;
    const url = `${CONFIG.API_URL}${CONFIG.SETTLEMENT_URL}`

    return this.http.post<any>(url, {
      //HARDCODED FOR NOW: merchantid and currency and bank transfer method

      "merchantId": id,
      "currency": "CNY",
      "amount": amt.toFixed(2),
      "note": note,
      "methodDetails": JSON.parse(param),

    }, { headers: headers }).map(res => {

      //this.router.navigate(['/settlement']);
      return res;
    },
      err => {
        this.handleError(err);
      });
  }

  createUser(username: string, password: string, email: string, firstname: string, lastname: string, merchantcodes: string, role: string) {
    const url = `${CONFIG.API_URL}${CONFIG.USERS_URL}`
    let merchants = new Array();
    let roleid: number;
    if (merchantcodes != null) {
      merchants.push(merchantcodes);
    }

    if (role == 'Administrator') {
      roleid = 1;
    }
    if (role == 'Merchant') {
      roleid = 2;
    }

    //this.role = +roleid;
    return this.http.post<any>(url, {
      "username": username,
      "password": password,
      "email": email,
      "firstName": firstname,
      "lastName": lastname,
      "merchantCodes": merchants,
      "roleId": roleid
    }).map(res => {
      //location.reload()
      return res;
    },
      err => {
        this.handleError(err);
      });
  }

  getUser(id: number): any {
    const url = `${CONFIG.API_URL}${CONFIG.USERS_URL}/${id}`;
    return this.http.get(url)
  }


  editUser(id: number, firstname: string, lastname: string, email: string, roleid: number) {
    const url = `${CONFIG.API_URL}${CONFIG.USERS_URL}/${id}`;
    let role = +roleid;
    return this.http.put<any>(url, {
      "firstName": firstname,
      "lastName": lastname,
      "email": email,
      "roleId": role
    }).map(res => {
      //this.router.navigate(['/users']);
      return res;
    },
      err => {
        this.handleError(err);
      });
  }

  disableUser(id: number) {
    const url = `${CONFIG.API_URL}${CONFIG.USERS_URL}/${id}/disable`;
    return this.http.put<any>(url, {}).subscribe(res => {
      this.router.navigate(['/users']);
    },
      err => {
        this.handleError(err);
      });
  }

  enableUser(id: number) {
    const url = `${CONFIG.API_URL}${CONFIG.USERS_URL}/${id}/enable`;
    return this.http.put<any>(url, {}).subscribe(res => {
      this.router.navigate(['/users']);
    },
      err => {
        this.handleError(err);
      });
  }

  getMerchant(id: number) {
    const url = `${CONFIG.API_URL}${CONFIG.MERCHANT_URL}/${id}`
    return this.http.get(url);
  }

  addMerchant(code: string, name: string, cny: string, thb: string) {
    const url = `${CONFIG.API_URL}${CONFIG.MERCHANT_URL}`
    let currencies = new Array();
    if (cny) {
      currencies.push("CNY");
    }
    if (thb) {
      currencies.push("THB");
    }
    return this.http.post<any>(url, {
      "code": code,
      "fullName": name,
      "currencies": currencies,
    }).map(res => {
      //location.reload()
      return res;
    },
      err => {
        this.handleError(err);
      });
  }

  addRate(id: number, effectivedatefrom: string, transactiontype: string, paymentmethod: string, percentage: string) {
    const url = `${CONFIG.API_URL}${CONFIG.MERCHANT_URL}/${id}/rates`
    return this.http.put<any>(url, {
      "transactionType": transactiontype,
      "paymentMethod": paymentmethod,
      "percentage": percentage,
      "effectiveDateFrom": effectivedatefrom,
    }).subscribe(res => {
      location.reload()
    },
      err => {
        this.catchError (err);
      });
  }

  addRate2(id: number, effectivedatefrom: string, transactiontype: string, paymentmethod: string, percentage: string) :any{
    const url = `${CONFIG.API_URL}${CONFIG.MERCHANT_URL}/${id}/rates`
    return this.http.put<any>(url, {
      "transactionType": transactiontype,
      "paymentMethod": paymentmethod,
      "percentage": percentage,
      "effectiveDateFrom": effectivedatefrom,
    }).map(res => {
      //this.notify("Add rate successful","success")
      return res;
    },
      err => {
        
        this.handleError(err);
      });
  }

  catchError(err) {
        this.notifyService.notify(err.error.error, 'error')
  }

  getCurrentRate(id: number, transactiontype: string, paymentMethod: string): any {
    const params = new HttpParams().set('transactionType', transactiontype).set('paymentMethod', paymentMethod);
    const url = `${CONFIG.API_URL}${CONFIG.MERCHANT_URL}/${id}/${CONFIG.CURRENT_RATES_URL}`
    return this.http.get(url, { params });
  }

  getAllRates(id: number, transactiontype: string, paymentMethod: string, page: number): any {
    const params = new HttpParams().set('transactionType', transactiontype).set('paymentMethod', paymentMethod).set('page', page.toString()).set('size', '10');
    const url = `${CONFIG.API_URL}${CONFIG.MERCHANT_URL}/${id}`+'/rates'
    return this.http.get(url, { params });
  }

  getRunningBalance(id: number) {
    const url = `${CONFIG.API_URL}${CONFIG.MERCHANT_URL}/${id}/wallets`
    return this.http.get(url);
  }

  getRoutingRule(id: number):any {
    const url = `${CONFIG.API_URL}${CONFIG.ROUTING_RULE_URL}/${id}`
    return this.http.get(url);
  }

  updateRoutingRule(id: number, post){
    const url = `${CONFIG.API_URL}${CONFIG.ROUTING_RULE_URL}/${id}`
    let mid = post.midconfigcode
    let weight = post.weight
    return this.http.put<any>(url, {
      
      "midConfigCode": mid,
      "weight": weight,
    }).map(res => {
      //this.notify("Routing Rule successfully updated","success")
        return res;
    },
      err => {
        this.handleError(err);
      });
  }

  updateRoutingRule2(id: number, post){
    const url = `${CONFIG.API_URL}${CONFIG.ROUTING_RULE_URL}/${id}`
    let mid = post.midconfigcode
    let weight = post.weight
    return this.http.put<any>(url, {
      
      "midConfigCode": mid,
      "weight": weight,
    })
  }

  updateMid(id: number, post){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/limits`
    let minamount = post.minamount
    let maxamount = post.maxamount
    let currency = post.currency
    return this.http.put<any>(url, {
      "currency": currency,
      "minAmount": minamount,
      "maxAmount": maxamount,
    }).map(res => {
      //this.notify("Mid successfully updated","success")
      return res;
    },
      err => {
        this.handleError(err);
      });
  }

  updateMid2(id: number, post){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/limits`
    let minamount = post.minamount
    let maxamount = post.maxamount
    let currency = post.currency
    return this.http.put<any>(url, {
      "currency": currency,
      "minAmount": minamount,
      "maxAmount": maxamount,
    })
  }

  updateMidDetails(id: number, post){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/int-details`
    let callbackurl = post.callbackurl
    let merchantid = post.merchantid
    let requesturl = post.requesturl
    let sign = post.sign
    let remarks = post.remarks
    let landingurl = post.landingurl
    let privatekey = post.privatekey

    return this.http.put<any>(url, {
      "CALLBACK_URL": callbackurl,
      "MID": merchantid,
      "REQUEST_URL": requesturl,
      "SIGN_KEY": sign,
      "REMARKS": remarks,
      "PRIVATE_KEY": privatekey,
      "LANDING_URL": landingurl,
    }).map(res => {
      //this.notify("Mid details successfully updated","success")
      return res;
    },
      err => {
        this.handleError(err);
      });
  }

  updateMidDetails2(id: number, post){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/int-details`
    let callbackurl = post.callbackurl
    let merchantid = post.merchantid
    let requesturl = post.requesturl
    let sign = post.sign
    let remarks = post.remarks
    let landingurl = post.landingurl
    let privatekey = post.privatekey

    return this.http.put<any>(url, {
      "CALLBACK_URL": callbackurl,
      "MID": merchantid,
      "REQUEST_URL": requesturl,
      "SIGN_KEY": sign,
      "REMARKS": remarks,
      "PRIVATE_KEY": privatekey,
      "LANDING_URL": landingurl,
    })
  }  

  enableRoutingRule(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.ROUTING_RULE_URL}/${id}/enable`
    return this.http.put<any>(url,{}).subscribe(res => {
      location.reload()
    },
      err => {
        this.handleError(err);
      });
  }
  
  enableRoutingRule2(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.ROUTING_RULE_URL}/${id}/enable`
    return this.http.put<any>(url,{})
  }

  disableRoutingRule(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.ROUTING_RULE_URL}/${id}/disable`
    return this.http.put<any>(url,{}).subscribe(res => {
      location.reload()
    },
      err => {
        this.handleError(err);
      });
  }

  disableRoutingRule2(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.ROUTING_RULE_URL}/${id}/disable`
    return this.http.put<any>(url,{})
  }

  deleteRoutingRule(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.ROUTING_RULE_URL}/${id}`
    return this.http.delete<any>(url).subscribe(res => {
      this.router.navigate(['/routing']);
    },
      err => {
        this.handleError(err);
      });
  }

  getMid(id: number):any {
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}`
    return this.http.get(url);
  }

  getMidField(id: number):any {
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/int-details`
    return this.http.get(url);
  }

  enableMid(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/enable`
    return this.http.put<any>(url,{}).subscribe(res => {
      location.reload()
    },
      err => {
        this.handleError(err);
      });
  }
  
  enableMid2(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/enable`
    return this.http.put<any>(url,{})
  }

  disableMid(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/disable`
    return this.http.put<any>(url,{}).subscribe(res => {
      location.reload()
    },
      err => {
        this.handleError(err);
      });
  }

  disableMid2(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/disable`
    return this.http.put<any>(url,{})
  }

  archiveMid(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/archive`
    return this.http.put<any>(url,{}).subscribe(res => {
      this.router.navigate(['/midmanagement']);
    },
      err => {
        this.handleError(err);
      });
  }

  archiveMid2(id:number){
    const url = `${CONFIG.API_URL}${CONFIG.MID_CONFIG_URL}/${id}/archive`
    return this.http.put<any>(url,{})
  }

  UpdateBalance(id: number, post) {
    let amount = post.amount;
    let currency = post.currency;
    const url = `${CONFIG.API_URL}${CONFIG.MERCHANT_URL}/${id}/wallets`
    return this.http.put<any>(url, {
      "currency": currency,
      "amount": amount,
    }).map(res => {
      //this.notify("Balance successfully updated","success")
      return res;
    },
      err => {
        this.handleError(err);
      });
  }

  UpdateBalance2(id: number, post) {
    let amount = post.amount;
    let currency = post.currency;
    const url = `${CONFIG.API_URL}${CONFIG.MERCHANT_URL}/${id}/wallets`
    return this.http.put<any>(url, {
      "currency": currency,
      "amount": amount,
    })
  }  


}